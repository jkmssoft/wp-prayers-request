<?php
/*
Plugin Name: WP Prayer II (modified)
Plugin URI: https://www.goministry.com/
Description: Prayer Management System
Version: 99
Author: Go Prayer modified by jkmssoft
Author URI: https://www.goprayer.com/
License: GPLv2 or later
*/

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Nope, not accessing this' );
}
define( 'UPR_PLUGIN', esc_html__( 'User Prayers Request', 'upr' ) );
define( 'UPR_PATH', plugin_basename( __FILE__ ) );

// include shortcodes
require plugin_dir_path( __FILE__ ) . 'inc/upr_shortcode.php';
add_action( 'admin_menu', 'upr_prayer_sub_menu' );

include_once ( 'inc/upr_post_exists.php' );

// REST
add_action( 'rest_api_init', function () {
  register_rest_route( 'prayers/v1', '/prayers', array(
    'methods' => 'GET',
    'callback' => 'upr_rest_get',
  ) );

  register_rest_route( 'prayers/v1', '/add-comment', array(
    'methods' => 'POST',
    'callback' => 'upr_rest_post_add_comment',
  ) );
  
  register_rest_route( 'prayers/v1', '/add-prayers', array(
    'methods' => 'POST',
    'callback' => 'upr_rest_post_add',
  ) );

  register_rest_route( 'prayers/v1', '/pray', array(
    'methods' => 'POST',
    'callback' => 'upr_rest_post_pray',
  ) );
} );


function upr_rest_post_add_comment () {
    global $current_user;
    get_currentuserinfo();
    $time             = current_time( 'mysql' );
    $comment_approved = 1;
    if ( get_option( 'comment_moderation' ) == 1 ) {
        $comment_approved = 0;
    }
    if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
        $user_ip = '127.0.0.1';
    } else {
        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            // check ip from share internet
            $user_ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            // to check ip is pass from proxy
            $user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $user_ip = $_SERVER['REMOTE_ADDR'];
        }
    }

    $user_agent   = $_SERVER['HTTP_USER_AGENT'];
    $comment_type = 'pray';
    if ( is_user_logged_in() ) {
        $author_user_id       = $current_user->ID;
        $comment_author       = $current_user->display_name;
        $comment_author_email = $current_user->user_email;
        $comment_content      = $_REQUEST['pray_reply'];
        $comment_author_url   = $current_user->user_url;
    } else {
        // todo not checked if valid email or website url
        $author_user_id       = '';
        $comment_author       = strip_tags( $_REQUEST['comment_author'] );
        $comment_author_email = strip_tags( $_REQUEST['comment_author_email'] );
        $comment_content      = strip_tags( $_REQUEST['pray_reply'] );
        $comment_author_url   = strip_tags( $_REQUEST['comment_author_url'] );
    }
    $data       = array(
        'comment_post_ID'      => $_REQUEST['prayer_id'],
        'comment_author'       => $comment_author,
        'user_id'              => $author_user_id,
        'comment_author_email' => $comment_author_email,
        'comment_content'      => $comment_content,
        'comment_type'         => 'pray',
        'comment_parent'       => $_REQUEST['comment_parent'],
        'comment_date'         => $time,
        'comment_approved'     => $comment_approved,
        'comment_author_url'   => $comment_author_url,
        'comment_author_IP'    => $user_ip,
        'comment_agent'        => $user_agent,
    );
    $comment_id = wp_insert_comment( $data );
    $comment    = get_comment( $comment_id );
    if ( $comment_id ) {
        $moderation_notify = get_option( 'moderation_notify' );
        $blacklisted       = wp_blacklist_check( $comment->comment_author, $comment->comment_author_email, $comment->comment_author_url, $comment->comment_content, $comment->comment_author_IP, $comment->comment_agent );
        if ( $blacklisted ) {
            wp_spam_comment( $comment_id );
        } else {
            if ( check_comment( $comment->comment_author, $comment->comment_author_email, $comment->comment_author_url, $comment->comment_content, $comment->comment_author_IP, $comment->comment_agent, $comment->comment_type ) ) {
                wp_set_comment_status( $comment_id, 1 );
            } else {
                // disable notification of website admin
                // wp_notify_moderator( $comment_id );
                wp_set_comment_status( $comment_id, 0 );

                // send email to admin ONLY of this plugin
                $upr_prayer_req_admin_email = get_option( 'prayer_req_admin_email' );
                if ( isset( $upr_prayer_req_admin_email ) && $upr_prayer_req_admin_email != '' ) {
                    $to_admin = $upr_prayer_req_admin_email;
                } else {
                    $to_admin = get_option( 'admin_email' );
                }
                if ( ! empty( $to_admin ) ) {
                    // todo Admin CC does not work here! add_filter('wp_mail',array($this,'custom_mails'), 10,1);
                    $subject = __( 'New Comment to Prayer Request to moderate', 'prayers' );

                    // $comment->comment_author_url
                    // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                    $headers  .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $body      = 'Hello, <br> <p>There is a new comment to a prayer request to moderate!<br>';
                    $body     .= '<b>Name:</b> ' . $comment_author . '<br>';
                    $body     .= '<b>Message:</b> ' . $comment_content . '<br>';
                    $body     .= '<b>URL:</b> ' . $comment_author_url . '<br>';
                    $actionUrl = admin_url( "comment.php?action=approve&c={$comment_id}#wpbody-content" );
                    $body     .= sprintf( __( 'Approve it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';

                    if ( EMPTY_TRASH_DAYS ) {
                        /* translators: Comment moderation. %s: Comment action URL. */
                        $actionUrl = admin_url( "comment.php?action=trash&c={$comment_id}#wpbody-content" );
                        $body     .= sprintf( __( 'Trash it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';
                    } else {
                        /* translators: Comment moderation. %s: Comment action URL. */
                        $actionUrl = admin_url( "comment.php?action=delete&c={$comment_id}#wpbody-content" );
                        $body     .= sprintf( __( 'Delete it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';
                    }
                    $actionUrl = admin_url( "comment.php?action=spam&c={$comment_id}#wpbody-content" );
                    $body     .= sprintf( __( 'Spam it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';
                    
                    $body .= '<br>Thank you';

                    wp_mail( $to_admin, $subject, $body, $headers );
                }
            }
        }
        return [
            'code' => 0,
            'message' => 'success',
            'comment_id' => $comment_id,
        ];
    } else {
        return [
            'code' => -1,
            'message' => 'error',
        ];
    }
}

function upr_rest_post_pray () {
    global $current_user, $wpdb;
    get_currentuserinfo();
    if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
        $ip = '127.0.0.1';
    } else {
        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            // check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            // to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
    }
    if ( is_user_logged_in() ) {
        $user_id = $current_user->display_name;
    } else {
        $user_id = $ip;
    }

    $prayer_id   = $_POST['prayer_id'];
    
    $prayer_post = array(
        'post_title'   => $user_id,
        'post_content' => $prayer_id,
        'post_type'    => 'prayers_performed',
        'post_status'  => 'publish',
    );
    // Insert the post into the database
    $post_id = wp_insert_post( $prayer_post );
    if ( $post_id ) {
        $prayer_count = (int)get_post_meta( $prayer_id, 'prayers_count', true );
        $prayer_count = $prayer_count + 1;
        update_post_meta( $prayer_id, 'prayers_count', $prayer_count );
        update_post_meta( $post_id, 'prayer_id', $prayer_id );
        update_post_meta( $post_id, 'user_id', $user_id );
            
        // save cookie
        if ( get_option( 'upr_disable_hide_all_prayed' ) == 0 ) {
            //setcookie( 'prayer_plugin', json_encode( $cookie ), time() + 60 * 60 * 24 * 1000, '/' ); // 1000days
                
            // store ids and count of prays
            if ( ! isset( $_COOKIE['prayer_requests'] ) ) {
                $prayer_requests = [];
            } else {
                $prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
            }
            if ( isset( $prayer_requests[ $prayer_id ] ) ) {
                $prayer_requests[ $prayer_id ]++;
            } else {
                $prayer_requests[ $prayer_id ] = 1;
            }

            $cookiedata = json_encode( $prayer_requests );
            if ( strlen($cookiedata) > 4000 ) {
                // then reset the cookie array
                $cookiedata = array();
            }

            setcookie( 'prayer_requests', $cookiedata, time() + 60 * 60 * 24 * 30, '/' ); // 30days
        }
        
        return [
            'code' => 0,
            'message' => 'success'
        ];
    }
    return [
        'code' => -1,
        'message' => 'fail'
    ];
}
// todo redundanter code hier und in upr_shortcode!
function upr_website_email( $sender ) {
    $prayer_email_user = get_option( 'prayer_email_user' );
    $sitename          = strtolower( $_SERVER['SERVER_NAME'] );
    if ( substr( $sitename, 0, 4 ) == 'www.' ) {
        $sitename = substr( $sitename, 4 );
    }
    $illegal_chars_username = array( '(', ')', '<', '>', ',', ';', ':', '\\', '"', '[', ']', '@', "'", ' ' );
    $username               = str_replace( $illegal_chars_username, '', get_option( 'blogname' ) );
    $sender_emailuser       = ( isset( $prayer_email_user ) and ! empty( $prayer_email_user ) ) ? $prayer_email_user : $username . '@' . $sitename;
    $sender_email           = $sender_emailuser;
		
    return $sender_email;
}

function upr_website_name( $name ) {
    // $prayer_email_from = unserialize(get_option('prayer_email_from'));
    $prayer_email_from = get_option( 'prayer_email_from' );
    $site_name         = ( isset( $prayer_email_from ) and ! empty( $prayer_email_from ) ) ? $prayer_email_from : get_option( 'blogname' );

    return $site_name;
}

	function custom_mails( $args ) {
		$copy_to = get_option( 'prayer_admin_email_cc' );

		$copy_to = explode( ',', $copy_to );
		foreach ( $copy_to as $cc_email ) {
			if ( ! empty( $cc_email ) ) {
				if ( is_array( $args['headers'] ) ) {
					$args['headers'][] = 'cc: ' . $cc_email;
				} else {
					$args['headers'] .= 'cc: ' . $cc_email . "\r\n";
				}
			}
		}
		return $args;
	}

function upr_rest_post_add () {
    if ( isset( $_REQUEST['type'] ) ) {
        $type = $_REQUEST['type'];
        if (!in_array($type, ['prophecies', 'witnesses', 'prayers'])) {
            return [
                'code' => -3,
                'message' => 'invalid type',
            ];
        }
        // Create post object
		// spam prevent
		if ( empty( $_REQUEST['prayer_messages'] )
            || empty( $_REQUEST['prayer_title'] )
            || empty( $_REQUEST['prayer_author_name'] )
        ) {
            return [
                'code' => -1,
                'message' => 'missing input',
            ];
        }
        
        $post_status                       = 'publish';
        $upr_prayer_default_status_pending = get_option( 'upr_prayer_default_status_pending' );
        if ( $upr_prayer_default_status_pending == 1 ) {
            $post_status = 'pending';
        }

        $upr_prayer_send_email = get_option( 'upr_prayer_send_email' );
        $_REQUEST['prayer_title'] = strip_tags( $_REQUEST['prayer_title'] );
        $_REQUEST['prayer_messages'] = strip_tags( $_REQUEST['prayer_messages'], '<br>' );
        $prayer_post           = array(
            'post_title'   => $_REQUEST['prayer_title'],
            'post_content' => $_REQUEST['prayer_messages'],
            'post_type'    => $type,
            'post_status'  => $post_status,
        );
        
        // check if X already exists
        $post_exists = upr_post_exists(
            $prayer_post['post_title'],
            $prayer_post['post_content'],
            '',
            $type
        );
        if ($post_exists > 0) {
            return [
                'code' => -2,
                'message' => 'already exists',
            ];
        }
                                                
        // Insert the post into the database
        if ($post_exists == 0) {
            $prayer_id                   = wp_insert_post( $prayer_post );
            $praylink                    = get_permalink( $prayer_id );
        }
        $upr_prayer_send_email       = get_option( 'upr_prayer_send_email' );
        $upr_prayer_send_admin_email = get_option( 'upr_prayer_send_admin_email' );
        if ( $prayer_id ) {
            if ( isset( $_REQUEST['prayer_author_category'] ) ) {
                $category = $_REQUEST['prayer_author_category'];
                wp_set_post_terms( $prayer_id, array( $category ), 'prayertypes', false );
            }
            
            update_post_meta( $prayer_id, 'prayers_name', wp_strip_all_tags( $_REQUEST['prayer_author_name'] ) );
            update_post_meta( $prayer_id, 'prayers_email', wp_strip_all_tags( $_REQUEST['prayer_author_email'] ) );
            if ( isset( $_REQUEST['prayer_author_country'] ) ) {
                update_post_meta( $prayer_id, 'prayers_country', $_REQUEST['prayer_author_country'] );
            }
            $to = $_REQUEST['prayer_author_email'];
            $prayer_email_from = get_option( 'prayer_email_from' );
            $prayer_email_user = get_option( 'prayer_email_user' );

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

            if ( $upr_prayer_send_email == 1 ) {
                if ( ! empty( $_REQUEST['prayer_author_email'] ) ) {
                    add_filter( 'wp_mail_from', 'upr_website_email' );
                    add_filter( 'wp_mail_from_name', 'upr_website_name' );
                    $body                     = '';
                    $prayer_email_req_subject = get_option( 'prayer_email_req_subject' );
                    if ( ! empty( $prayer_email_req_subject ) ) {
                        $subject = $prayer_email_req_subject;
                    } else {
                        $subject = 'Prayer request confirmation';
                    }
                    $prayer_email_req_messages = get_option( 'prayer_email_req_messages' );
                    if ( ! empty( $prayer_email_req_messages ) ) {
                        $body = $prayer_email_req_messages;
                        $body = str_replace(
                            array( '{prayer_author_name}' ),
                            array( $_REQUEST['prayer_author_name'] ),
                            $body
                        );
                    } else {
                        // todo translation!
                        $body  = 'Hello ' . $_REQUEST['prayer_author_name'] . ', <br> <p>Thank you for submitting your prayer request.<br />';
                        $body .= 'We welcome all requests and we delight in lifting you and your requests up to God in prayer.<br />';
                        $body .= 'God Bless you, and remember, God knows the prayers that are coming and hears them even before they are spoken.<br />';
                        $body .= '<a href="' . $praylink . '">' . $praylink . '</a>';
                        $body .= '<br /><br />Blessings,<br/ >Prayer Team</p>';
                    }
                    wp_mail( $to, $subject, $body, $headers );
                }
            }

            if ( $upr_prayer_send_admin_email == 1 ) {
                $upr_prayer_req_admin_email = get_option( 'prayer_req_admin_email' );
                
                if ( isset( $upr_prayer_req_admin_email ) && $upr_prayer_req_admin_email != '' ) {
                    $to_admin = $upr_prayer_req_admin_email;
                } else {
                    $to_admin = get_option( 'admin_email' );
                }
                if ( ! empty( $to_admin ) ) {
                    add_filter( 'wp_mail_from', 'upr_website_email' );
                    add_filter( 'wp_mail_from_name', 'upr_website_name' );
                    add_filter( 'wp_mail', 'custom_mails', 10, 1 );
                    $body                       = '';
                    $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                    if ( ! empty( $prayer_email_admin_subject ) ) {
                        $subject = $prayer_email_admin_subject;
                    } else {
                        $subject = 'New {request_type} received';
                    }
                    $subject = str_replace( array( '{request_type}' ), array( 'Prayer Request' ), $subject );
                    // todo:: $data array missing
                    // $prayer_author_info = get_userdata($data['prayer_author']);
                    // $to = $prayer_author_info->user_email;
                    $prayer_email_admin_messages = get_option( 'prayer_email_admin_messages' );

                    if ( ! empty( $prayer_email_admin_messages ) ) {
                        $body = $prayer_email_admin_messages;
                        $body = str_replace(
                            array(
                                '{prayer_author_name}',
                                '{prayer_author_email}',
                                '{prayer_title}',
                                '{prayer_messages}',
                                '{prayer_author_info}',
                                '{request_type}',
                            ),
                            array(
                                $_REQUEST['prayer_author_name'],
                                $_REQUEST['prayer_author_email'],
                                $_REQUEST['prayer_title'],
                                $_REQUEST['prayer_messages'],
                                $_REQUEST['prayer_author_name'],
                                'Prayer Request',
                            ),
                            $body
                        );
                    } else {
                        if ( $type == 'prophecies' ) {
                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                $subject = $prayer_email_admin_subject;
                            } else {
                                $subject = 'New Prophecy Received To Moderate';
                            }
                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                            // todo translation!
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $body     = 'Hello, <br> <p>You have received a new ';
                            $body    .= 'prophecy to moderate with following details:</p>';
                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                            // todo:: $data array missing
                            // if($data['prayer_author_email']!='')
                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                        } elseif ( $type == 'prayers' ) {
                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                $subject = $prayer_email_admin_subject;
                            } else {
                                $subject = 'New Prayer Request Received To Moderate';
                            }
                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $body     = 'Hello, <br> <p>You have received a new ';
                            $body    .= 'prayer request';
                            $body    .= ' to moderate with following details:</p>';
                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                            // todo:: $data array missing
                            // if($data['prayer_author_email']!='')
                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                        } elseif ( $type == 'witnesses' ) {
                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                $subject = $prayer_email_admin_subject;
                            } else {
                                $subject = 'New Witness Received To Moderate';
                            }
                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $body     = 'Hello, <br> <p>You have received a new ';
                            $body    .= 'witness';
                            $body    .= ' to moderate with following details:</p>';
                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                            // todo:: $data array missing
                            // if($data['prayer_author_email']!='')
                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                        }
                        $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                        $body .= '<b>Message:</b> ' . $_REQUEST['prayer_messages'] . '<br>';
                        $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                        $body .= '<br>Thank you';
                    }
                    wp_mail( $to_admin, $subject, $body, $headers );
                }
            } // admin mail
            
            return [
                'code' => 0,
                'message' => 'success',
            ]; 
        }
    }
    return [
        'code' => -4,
        'message' => 'type missing',
    ];
}

function upr_rest_get () {
    global $wp_query, $wpdb;

	wp_reset_query();

	// find prayer requests
	$upr_no_prayer_per_page = get_option( 'upr_no_prayer_per_page' );

	$paged = intval( !isset( $_GET['paged'] ) ? 1 : $_GET['paged']);//( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	if ( $upr_no_prayer_per_page < 0 || $upr_no_prayer_per_page == '' ) {
		$upr_no_prayer_per_page = 10;
	}

	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
    
    $typeIcons = [
        'witnesses' => '<span>&#128488;</span>',
        'prophecies' => '<span>&#11088;</span>',
        'prayers' => '<span>&#128591;</span>',
    ];
    
    $upr_show_do_pray = true;

    // get types
    $types = isset($_GET['type']) ? $_GET['type'] : ['prophecies', 'witnesses', 'prayers'];
    if ( !is_array( $types ) ) {
        $types = [$types];
    }

	$query = '
        SELECT *
        FROM ' . $wpdb->posts . ' p
        WHERE post_status = "publish"
        AND (';
    $typeQuery = '';
    $i = 0;
    if ( empty( $types ) ) {
        $types = ['prophecies', 'witnesses', 'prayers'];
    }
    foreach ( $types as $type ) {
        if ( in_array( $type, ['prophecies', 'witnesses', 'prayers'] ) ) {
            if ( $i > 0 ) {
                $typeQuery .= ' OR ';
            }
            $typeQuery .= ' post_type = "' . $type . '"';
            $i++;
        }
    }

    $query .= $typeQuery . ')';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
    $queryPostDate = '';
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$queryPostDate = ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
		$query .= $queryPostDate;
	}

    // hide the prayed items
    $queryHidePrayed = '';
    if ( isset( $_GET['hidePrayed'] ) ) {
        if ( ! isset( $_COOKIE['prayer_requests'] ) ) {
            $pr_ids = array();
        } else {
            $cookie_prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
            $pr_ids = array_keys( $cookie_prayer_requests );
            // security, only ints
            foreach ( $pr_ids as &$id ) {
                $id = intval($id);
            }
        }
        if ( ! empty( $pr_ids ) ) {
            $queryHidePrayed = ' AND ID NOT IN (' . implode( ', ', $pr_ids ) .')';
        }
    }
    $query .= $queryHidePrayed;

    $queryBeforeLimit .= $query . ' ORDER BY post_date DESC';

    $query = $queryBeforeLimit
        . ' LIMIT ' . intval(($paged-1) * $upr_no_prayer_per_page) . ', ' . $upr_no_prayer_per_page;
	$queryResult = $wpdb->get_results( $query );

    // if empty, then go to startpage
    if ( empty( $queryResult ) ) {
        $query = $queryBeforeLimit
        . ' LIMIT ' . intval(0 * $upr_no_prayer_per_page) . ', ' . $upr_no_prayer_per_page;
    
        $queryResult = $wpdb->get_results( $query );
        $paged = 1;
    }

    // show message if there are no items
    if ( empty( $queryResult ) ) {
        return [
            'code' => -1,
            'message' => 'no data',
        ];
    }

    $a_prayers = array();

	// if we have prayers
	if ( $queryResult ) {
		$current_url = get_permalink( get_the_ID() );
        
		// foreach prayer request
		$registration = get_option( 'comment_registration' );
		global $current_user;
		get_currentuserinfo();
		if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
			$ip = '127.0.0.1';
		} else {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				// check ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				// to check ip is pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}
		if ( is_user_logged_in() ) {
			$uid     = $current_user->ID;
			$user_id = $current_user->display_name;
		} else {
			$uid = $current_user->ID;
		}

		$upr_hide_prayer_button               = get_option( 'upr_hide_prayer_button' );
		$upr_prayer_login_required_prayer     = get_option( 'upr_prayer_login_required_prayer' );
		$upr_hide_prayer_count                = get_option( 'upr_hide_prayer_count' );
		$upr_display_username                 = get_option( 'upr_display_username_on_prayer_listing' );
		$upr_allow_comments_prayer_request    = get_option( 'upr_allow_comments_prayer_request' );
		$upr_pray_prayed_button_ip            = get_option( 'upr_pray_prayed_button_ip' );
		$upr_time_interval_pray_prayed_button = get_option( 'upr_time_interval_pray_prayed_button' );

        if ( $type == 'prophecies' || $type == 'witnesses' ) {
            $upr_hide_prayer_button               = 1;// get_option('upr_hide_prayer_button');
            $upr_hide_prayer_count                = 1;// get_option('upr_hide_prayer_count');
            $upr_allow_comments_prayer_request    = 0;// get_option('upr_allow_comments_prayer_request');
        }

		if ( $upr_prayer_login_required_prayer != 1 ) {
			if ( is_user_logged_in() ) {
				$upr_show_do_pray = true;
			}
		} else {
			$upr_show_do_pray = true;
		}

		foreach ( $queryResult as $prayer ) {
            $a_prayer = array();

			$wp_prayer_id        = $prayer->ID;
			$wp_prayer_title     = get_the_title( $wp_prayer_id );
			$prayer_date         = get_the_date( 'l, j. F Y', $wp_prayer_id );
			// $wp_prayer_content = apply_filters('the_content', $prayer->post_content);
			$wp_prayer_content = $prayer->post_content;

			if ( ! empty( $wp_prayer_content ) ) {
				$wp_prayer_content = strip_shortcodes( $wp_prayer_content );
			}

			$wp_prayer_permalink = get_permalink( $wp_prayer_id );
			$wp_prayers_name     = get_post_meta( $wp_prayer_id, 'prayers_name', true );
			$wp_prayers_email    = get_post_meta( $wp_prayer_id, 'prayers_email', true );
			$wp_prayers_website  = get_post_meta( $wp_prayer_id, 'prayers_website', true );
			$wp_prayers_count    = 0;
			if ( get_post_meta( $wp_prayer_id, 'prayers_count', true ) > 0 ) {
				$wp_prayers_count = get_post_meta( $wp_prayer_id, 'prayers_count', true );
			}

            $a_prayer['prayer_id'] = $prayer->ID;
            $a_prayer['type'] = $prayer->post_type;
            $a_prayer['title'] = get_the_title( $wp_prayer_id );
            $a_prayer['date'] = get_the_date( 'l, j. F Y', $wp_prayer_id );
            $a_prayer['content'] = $wp_prayer_content;
            $a_prayer['prayers_count'] = $wp_prayers_count;

            // comments
            $comments = get_comments(
                array(
                    'post_id' => $wp_prayer_id,
                    'status'  => 'approve',
                )
            );
            $a_comments = array();
            foreach ( $comments as $c ) {
                $a_comment = array();
                $a_comment['comment_ID'] = $c->comment_ID;
                $a_comment['comment_author'] = get_option( 'upr_display_username_on_prayer_listing' ) ? $c->comment_author : null;
                $a_comment['comment_content'] = $c->comment_content;
                $a_comment['comment_date'] = $c->comment_date;
                
                $a_comments[] = $a_comment;
            }

            $a_prayer['comments'] = $a_comments;
            $a_prayers['prayers'][] = $a_prayer;
		}
	}
    
    // pagination
    $query = '
        SELECT count(*)
        FROM ' . $wpdb->posts . ' p
        WHERE post_status = "publish"
        AND (';

    $query .= $typeQuery . ')';
    $query .= $queryPostDate;
    $query .= $queryHidePrayed;

    $count_total = $wpdb->get_var( $query );
    $max_num_pages   = ceil( $count_total / $upr_no_prayer_per_page );
    if ( $max_num_pages > 1 ) {
        $a_prayers['pagination']['pages'] = $max_num_pages;
        $a_prayers['pagination']['current_page'] = $paged;
    }

	return $a_prayers;
}

function upr_comments_rest () {
    $a_comment = array();



    return $a_comment;
}

class user_prayers_request {

	// magic function (triggered on initialization)
	public function __construct() {
		add_action( 'init', array( $this, 'upr_prayers_management_init' ) ); // register prayers content type
		add_action( 'add_meta_boxes', array( $this, 'upr_add_prayers_meta_boxes' ) ); // add meta boxes
		add_action( 'save_post_prayers', array( $this, 'upr_save_prayers' ) ); // save prayers
		add_action( 'wp_enqueue_scripts', array( $this, 'upr_enqueue_public_scripts_and_styles' ) ); // public scripts and styles

		register_activation_hook( __FILE__, array( $this, 'upr_plugin_activate' ) ); // activate hook
		register_deactivation_hook( __FILE__, array( $this, 'upr_plugin_deactivate' ) ); // deactivate hook
		add_action( 'wp_ajax_ajax_pray_response', array( $this, 'upr_ajax_pray_response' ) );
		add_action( 'wp_ajax_nopriv_ajax_pray_response', array( $this, 'upr_ajax_pray_response' ) );
		add_action( 'wp_ajax_ajax_do_pray', array( $this, 'upr_ajax_do_pray' ) );
		add_action( 'wp_ajax_nopriv_ajax_do_pray', array( $this, 'upr_ajax_do_pray' ) );

		// edit column
		add_filter( 'manage_prayers_posts_columns', array( $this, 'upr_set_custom_edit_prayers_columns' ) );
		add_action( 'manage_prayers_posts_custom_column', array( $this, 'upr_custom_prayers_column' ), 10, 2 );
	}
	function upr_set_custom_edit_prayers_columns( $columns ) {
			unset( $columns['author'] );
			unset( $columns['comments'] );
			unset( $columns['date'] );
			unset( $columns['taxonomy-prayertypes'] );
			$columns['prayers_name']         = __( 'Author' );
			$columns['taxonomy-prayertypes'] = __( 'Categories' );
			$columns['date']                 = __( 'Date' );
			$columns['comments']             = __( 'Comments' );
			return $columns;
	}
	function upr_custom_prayers_column( $column, $post_id ) {
		switch ( $column ) {
			case 'prayers_name':
				$post                = get_post( $post_id );
				$author_id           = $post->post_author;
				$post_author         = get_user_by( 'ID', $author_id );
				$author_display_name = $post_author->display_name;
				if ( $author_display_name != '' ) {
					$authorLink = get_author_posts_url( $author_id );
					echo '<a href="' . $authorLink . '" target="_blank">' . $author_display_name . '</a>';
				} else {
					$author = get_post_meta( $post_id, 'prayers_name', true );
					echo ucwords( $author );
				}
				break;
		}
	}
	function upr_ajax_do_pray() {
		global $current_user, $wpdb;
		get_currentuserinfo();
		if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
			$ip = '127.0.0.1';
		} else {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				// check ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				// to check ip is pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}
		if ( is_user_logged_in() ) {
			$user_id = $current_user->display_name;
		} else {
			$user_id = $ip;
		}
        // todo change to POST? ajax?
		$prayer_id   = $_POST['prayer_id'];
        
        /*
        $post = get_post( $prayer_id );
        $cookie = json_decode( stripslashes( $_COOKIE['prayer_plugin'] ), true );
        if (in_array($post->post_type, ['witnesses', 'prophecies'])) {
            if (in_array($prayer_id, $cookie[$post->post_type])) {
                die ('already');
            } else {
                $cookie[$post->post_type][] = $prayer_id;
            }
        }*/
        
		$prayer_post = array(
			'post_title'   => $user_id,
			'post_content' => $prayer_id,
			'post_type'    => 'prayers_performed',
			'post_status'  => 'publish',
		);
		// Insert the post into the database
		$post_id = wp_insert_post( $prayer_post );
		if ( $post_id ) {
			$prayer_count = (int)get_post_meta( $prayer_id, 'prayers_count', true );
			$prayer_count = $prayer_count + 1;
			update_post_meta( $prayer_id, 'prayers_count', $prayer_count );
			update_post_meta( $post_id, 'prayer_id', $prayer_id );
			update_post_meta( $post_id, 'user_id', $user_id );
            
            // save cookie
            if ( get_option( 'upr_disable_hide_all_prayed' ) == 0 ) {
                //setcookie( 'prayer_plugin', json_encode( $cookie ), time() + 60 * 60 * 24 * 1000, '/' ); // 1000days
                // store ids and count of prays
                if ( ! isset( $_COOKIE['prayer_requests'] ) ) {
                    $prayer_requests = [];
                } else {
                    $prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
                }
                if ( isset( $prayer_requests[ $prayer_id ] ) ) {
                    $prayer_requests[ $prayer_id ]++;
                } else {
                    $prayer_requests[ $prayer_id ] = 1;
                }

                $cookiedata = json_encode( $prayer_requests );
                if ( strlen($cookiedata) > 4000 ) {
                    // then reset the cookie array
                    $cookiedata = array();
                }

                setcookie( 'prayer_requests', $cookiedata, time() + 60 * 60 * 24 * 30, '/' ); // 30days
            }

			die( 'Prayed' );
		}
	}
	function upr_ajax_pray_response() {
		global $current_user;
		get_currentuserinfo();
		$time             = current_time( 'mysql' );
		$comment_approved = 1;
		if ( get_option( 'comment_moderation' ) == 1 ) {
			$comment_approved = 0;
		}
		if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
			$user_ip = '127.0.0.1';
		} else {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				// check ip from share internet
				$user_ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				// to check ip is pass from proxy
				$user_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$user_ip = $_SERVER['REMOTE_ADDR'];
			}
		}

		$user_agent   = $_SERVER['HTTP_USER_AGENT'];
		$comment_type = 'pray';
		if ( is_user_logged_in() ) {
			$author_user_id       = $current_user->ID;
			$comment_author       = $current_user->display_name;
			$comment_author_email = $current_user->user_email;
			$comment_content      = $_REQUEST['pray_reply'];
			$comment_author_url   = $current_user->user_url;
		} else {
			// todo not checked if valid email or website url
			$author_user_id       = '';
			$comment_author       = strip_tags( $_REQUEST['comment_author'] );
			$comment_author_email = strip_tags( $_REQUEST['comment_author_email'] );
			$comment_content      = strip_tags( $_REQUEST['pray_reply'] );
			$comment_author_url   = strip_tags( $_REQUEST['comment_author_url'] );
		}
		$data       = array(
			'comment_post_ID'      => $_REQUEST['prayer_id'],
			'comment_author'       => $comment_author,
			'user_id'              => $author_user_id,
			'comment_author_email' => $comment_author_email,
			'comment_content'      => $comment_content,
			'comment_type'         => 'pray',
			'comment_parent'       => $_REQUEST['comment_parent'],
			'comment_date'         => $time,
			'comment_approved'     => $comment_approved,
			'comment_author_url'   => $comment_author_url,
			'comment_author_IP'    => $user_ip,
			'comment_agent'        => $user_agent,
		);
		$comment_id = wp_insert_comment( $data );
		$comment    = get_comment( $comment_id );
		if ( $comment_id ) {
			$moderation_notify = get_option( 'moderation_notify' );
			$blacklisted       = wp_blacklist_check( $comment->comment_author, $comment->comment_author_email, $comment->comment_author_url, $comment->comment_content, $comment->comment_author_IP, $comment->comment_agent );
			if ( $blacklisted ) {
				wp_spam_comment( $comment_id );
			} else {
				if ( check_comment( $comment->comment_author, $comment->comment_author_email, $comment->comment_author_url, $comment->comment_content, $comment->comment_author_IP, $comment->comment_agent, $comment->comment_type ) ) {
					wp_set_comment_status( $comment_id, 1 );
				} else {
					// disable notification of website admin
					// wp_notify_moderator( $comment_id );
					wp_set_comment_status( $comment_id, 0 );

					// send email to admin ONLY of this plugin
					$upr_prayer_req_admin_email = get_option( 'prayer_req_admin_email' );
					if ( isset( $upr_prayer_req_admin_email ) && $upr_prayer_req_admin_email != '' ) {
						$to_admin = $upr_prayer_req_admin_email;
					} else {
						$to_admin = get_option( 'admin_email' );
					}
					if ( ! empty( $to_admin ) ) {
						// todo Admin CC does not work here! add_filter('wp_mail',array($this,'custom_mails'), 10,1);
						$subject = __( 'New Comment to Prayer Request to moderate', 'prayers' );

						// $comment->comment_author_url
						// $headers  .= 'MIME-Version: 1.0' . "\r\n";
						$headers  .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$body      = 'Hello, <br> <p>There is a new comment to a prayer request to moderate!<br>';
						$body     .= '<b>Name:</b> ' . $comment_author . '<br>';
						$body     .= '<b>Message:</b> ' . $comment_content . '<br>';
						$body     .= '<b>URL:</b> ' . $comment_author_url . '<br>';
						$actionUrl = admin_url( "comment.php?action=approve&c={$comment_id}#wpbody-content" );
						$body     .= sprintf( __( 'Approve it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';

						if ( EMPTY_TRASH_DAYS ) {
							/* translators: Comment moderation. %s: Comment action URL. */
							$actionUrl = admin_url( "comment.php?action=trash&c={$comment_id}#wpbody-content" );
							$body     .= sprintf( __( 'Trash it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';
						} else {
							/* translators: Comment moderation. %s: Comment action URL. */
							$actionUrl = admin_url( "comment.php?action=delete&c={$comment_id}#wpbody-content" );
							$body     .= sprintf( __( 'Delete it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';
						}
						$actionUrl = admin_url( "comment.php?action=spam&c={$comment_id}#wpbody-content" );
						$body     .= sprintf( __( 'Spam it: %s' ), '<a href="' . $actionUrl . '">' . $actionUrl . '</a>' ) . '<br>';

						$body .= '<br>Thank you';

						wp_mail( $to_admin, $subject, $body, $headers );
					}
				}
			}
			echo $comment_id;
		} else {
			echo 'Error';
		}
		exit;
	}

	// register the prayers custom post type
	public function upr_prayers_management_init() {
		load_plugin_textdomain( 'prayers', false, basename( dirname( __FILE__ ) ) . '/lang' );
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => __( 'Categories' ),
			'singular_name'     => __( 'Category' ),
			'search_items'      => __( 'Search Category' ),
			'all_items'         => __( 'All Categories' ),
			'parent_item'       => __( 'Parent Category' ),
			'parent_item_colon' => __( 'Parent Category' ),
			'edit_item'         => __( 'Edit Category' ),
			'update_item'       => __( 'Update Category' ),
			'add_new_item'      => __( 'Add New Category' ),
			'new_item_name'     => __( 'New Category Name' ),
			'menu_name'         => __( 'Categories' ),
		);
		$args   = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'genre' ),
		);
		register_taxonomy( 'prayertypes', array( 'prayers' ), $args );

		$labels = array(
			'name'               => __( 'Prayers', 'prayers' ),
			'singular_name'      => __( 'Prayers', 'prayers' ),
			'menu_name'          => __( 'Prayers Requests', 'prayers' ),
			'name_admin_bar'     => __( 'Manage Prayers', 'prayers' ),
			'add_new'            => __( 'Add Prayer', 'prayers' ),
			'add_new_item'       => __( 'Title', 'prayers' ),
			'new_item'           => __( 'Add Prayer', 'prayers' ),
			'edit_item'          => __( 'Title', 'prayers' ),
			'view_item'          => __( 'View Pray', 'prayers' ),
			'all_items'          => __( 'List Prayers', 'prayers' ),
			'search_items'       => __( 'Search', 'prayers' ),
			'parent_item_colon'  => __( 'Parent Pray', 'prayers' ),
			'not_found'          => __( 'No Pray Found', 'prayers' ),
			'not_found_in_trash' => __( 'No pray found in trash', 'prayers' ),
		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'prayers' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'menu_icon'          => plugins_url( 'assets/images/pray.png', __FILE__ ),
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'author', 'comments' ),
		);
		register_post_type( 'prayers', $args );

		// Prophecy
		$labels = array(
			'name'               => __( 'Prophecies', 'prayers' ),
			'singular_name'      => __( 'Prophecies', 'prayers' ),
			'menu_name'          => __( 'Prophecies Reports', 'prayers' ),
			'name_admin_bar'     => __( 'Manage Prophecies', 'prayers' ),
			'add_new'            => __( 'Add Prophecy', 'prayers' ),
			'add_new_item'       => __( 'Title', 'prayers' ),
			'new_item'           => __( 'Add Prophecy', 'prayers' ),
			'edit_item'          => __( 'Title', 'prayers' ),
			'view_item'          => __( 'View Pray', 'prayers' ),
			'all_items'          => __( 'List Prophecies', 'prayers' ),
			'search_items'       => __( 'Search', 'prayers' ),
			'parent_item_colon'  => __( 'Parent Prophecy', 'prayers' ),
			'not_found'          => __( 'No Prophecy Found', 'prayers' ),
			'not_found_in_trash' => __( 'No Prophecy found in trash', 'prayers' ),
		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
            // to show it in prayer request menu: 'show_in_menu'       => 'edit.php?post_type=prayers',
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'prophecies' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'menu_icon'          => plugins_url( 'assets/images/pray.png', __FILE__ ),
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'author', 'comments' ),
		);
		register_post_type( 'prophecies', $args );

		// Witness
		$labels = array(
			'name'               => __( 'Witnesses', 'prayers' ),
			'singular_name'      => __( 'Witnesses', 'prayers' ),
			'menu_name'          => __( 'Witnesses Reports', 'prayers' ),
			'name_admin_bar'     => __( 'Manage Witnesses', 'prayers' ),
			'add_new'            => __( 'Add Witness', 'prayers' ),
			'add_new_item'       => __( 'Title', 'prayers' ),
			'new_item'           => __( 'Add Witness', 'prayers' ),
			'edit_item'          => __( 'Title', 'prayers' ),
			'view_item'          => __( 'View Pray', 'prayers' ),
			'all_items'          => __( 'List Witnesses', 'prayers' ),
			'search_items'       => __( 'Search', 'prayers' ),
			'parent_item_colon'  => __( 'Parent Witness', 'prayers' ),
			'not_found'          => __( 'No Witness Found', 'prayers' ),
			'not_found_in_trash' => __( 'No Witness found in trash', 'prayers' ),
		);
		$args   = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'prophecies' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'menu_icon'          => plugins_url( 'assets/images/pray.png', __FILE__ ),
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'thumbnail', 'author', 'comments' ),
		);
		register_post_type( 'witnesses', $args );

		// perforamnce
		$labels = array(
			'name'           => __( 'Prayers Performed', 'prayers' ),
			'singular_name'  => __( 'Prayers Performed', 'prayers' ),
			'menu_name'      => __( 'Prayers Performed', 'prayers' ),
			'name_admin_bar' => __( 'Prayers Performed', 'prayers' ),
			'search_items'   => __( 'Search', 'prayers' ),
		);
		$args   = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => 'edit.php?post_type=prayers',
			'query_var'          => false,
			'rewrite'            => array( 'slug' => 'prayers_performed' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor' ),
		);
		register_post_type( 'prayers_performed', $args );

		// add options
		add_option( 'upr_prayer_list_title', 'Prayers' );
		add_option( 'upr_no_prayer_per_page', 10 );
		add_option( 'upr_login_not_required_request', 0 );
		add_option( 'upr_prayer_login_required_prayer', 0 );
		add_option( 'upr_prayer_send_email', 1 );
		add_option( 'upr_prayer_send_admin_email', 1 );
		add_option( 'upr_prayer_default_status_pending', 0 );
		add_option( 'upr_hide_prayer_button', 0 );
		add_option( 'upr_hide_prayer_count', 0 );
		add_option( 'upr_display_username_on_prayer_listing', 0 );
		add_option( 'upr_prayer_hide_captcha', 0 );
		add_option( 'upr_prayer_spam_prevention', 1 );
		add_option( 'upr_prayer_spam_DDoS_prevention', 1 );
		add_option( 'upr_prayer_show_country', 0 );
		add_option( 'upr_show_prayer_category', 0 );
		add_option( 'upr_time_interval_pray_prayed_button', 0 );
		add_option( 'upr_disable_ip_logging', 0 );
		add_option( 'upr_prayer_page_slug', '' );
		add_option( 'upr_use_local_emojis', 0 );
		add_option( 'upr_disable_hide_all_prayed', 0 );
	}
	// adding meta boxes for the prayer content type*/
	public function upr_add_prayers_meta_boxes() {
		add_meta_box(
			'wp_prayer_meta_box', // id
			__( 'Details' ), // name
			array( $this, 'upr_prayers_meta_box_display' ), // display function
			'prayers', // post type
			'side', // location
			'high' // priority
		);
		add_meta_box(
			'wp_prayer_meta_box', // id
			__( 'Details' ), // name
			array( $this, 'upr_prayers_meta_box_display' ), // display function
			'prophecies', // post type
			'side', // location
			'high' // priority
		);
		add_meta_box(
			'wp_prayer_meta_box', // id
			__( 'Details' ), // name
			array( $this, 'upr_prayers_meta_box_display' ), // display function
			'witnesses', // post type
			'side', // location
			'high' // priority
		);
	}

	// display function used for our custom prayer meta box*/
	public function upr_prayers_meta_box_display( $post ) {
		// set nonce field
		wp_nonce_field( 'prayers_nonce', 'prayers_nonce_field' );

		// collect variables
		$prayers_name    = get_post_meta( $post->ID, 'prayers_name', true );
		$prayers_email   = get_post_meta( $post->ID, 'prayers_email', true );
		$prayers_website = get_post_meta( $post->ID, 'prayers_website', true );
		$prayers_count   = get_post_meta( $post->ID, 'prayers_count', true );
		$prayers_country = get_post_meta( $post->ID, 'prayers_country', true );
		?>
		<div class="field-container">
			<?php
			// before main form elementst hook
			do_action( 'upr_prayers_admin_form_start' );
			?>
			<div class="field">
				<p><strong><?php _e( 'Name', 'prayers' ); ?></strong><br>
				<input type="text" name="prayers_name" size="30" id="prayers_name" value="<?php echo $prayers_name; ?>"/></p>
			</div>
			<div class="field">
				<p><strong><?php _e( 'Email', 'prayers' ); ?></strong><br>
				<input type="email" name="prayers_email"  size="30" id="prayers_email" value="<?php echo $prayers_email; ?>"/></p>
			</div>
			<div class="field">
				<p><strong><?php _e( 'Website', 'prayers' ); ?></strong><br>
				<input type="text" name="prayers_website" size="30" id="prayers_website" value="<?php echo $prayers_website; ?>"/></p>
			</div>
			<div class="field">
				<p><strong><?php _e( 'Prayer Count', 'prayers' ); ?></strong><br>
				<input type="number" name="prayers_count" size="30" id="prayers_count" value="<?php echo $prayers_count; ?>"/></p>
			</div>
		<?php
		// after main form elementst hook
		do_action( 'upr_prayers_admin_form_end' );
		?>
		</div>
		<?php
	}

	// triggered on activation of the plugin (called only once)
	public function upr_plugin_activate() {
		// call our custom content type function
		$this->upr_prayers_management_init();
		// flush permalinks
		flush_rewrite_rules();
	}

	// triggered when adding or editing a prayer
	public function upr_save_prayers( $post_id ) {
		// check for nonce
		if ( ! isset( $_POST['prayers_nonce_field'] ) ) {
			return $post_id;
		}
		// verify nonce
		if ( ! wp_verify_nonce( $_POST['prayers_nonce_field'], 'prayers_nonce' ) ) {
			return $post_id;
		}
		// check for autosave
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		// get our name, email and website fields
		$prayers_name    = isset( $_POST['prayers_name'] ) ? sanitize_text_field( $_POST['prayers_name'] ) : '';
		$prayers_email   = isset( $_POST['prayers_email'] ) ? sanitize_email( $_POST['prayers_email'] ) : '';
		$prayers_website = isset( $_POST['prayers_website'] ) ? esc_url( $_POST['prayers_website'] ) : '';
		$prayers_country = isset( $_POST['prayers_country'] ) ? sanitize_text_field( $_POST['prayers_country'] ) : '';

		// update phone, memil and address fields
		update_post_meta( $post_id, 'prayers_name', $prayers_name );
		update_post_meta( $post_id, 'prayers_email', $prayers_email );
		update_post_meta( $post_id, 'prayers_website', $prayers_website );
		update_post_meta( $post_id, 'prayers_country', $prayers_country );

		// prayers save hook
		// used so you can hook here and save additional post fields added
		do_action( 'prayers_admin_save', $post_id, $_POST );
	}

	// enqueues scripts and styled on the front end
	public function upr_enqueue_public_scripts_and_styles() {
		wp_enqueue_script( 'jquery' );
		global $post;
		if ( is_a( $post, 'WP_Post' )
            && has_shortcode( $post->post_content, 'upr_list_prayers' ) 
            || has_shortcode( $post->post_content, 'upr_form' )) {
			wp_enqueue_script( 'upr_scripts', plugin_dir_url( __FILE__ ) . '/assets/upr_prayers.js' );
		}
        if (
            has_shortcode( $post->post_content, 'upr_list_all' )
            || has_shortcode( $post->post_content, 'upr_form_all' ) ) {
			wp_enqueue_script( 'upr_scripts', plugin_dir_url( __FILE__ ) . '/assets/upr_prayers_all.js' );
        }
		wp_enqueue_style( 'upr_frontend', plugin_dir_url( __FILE__ ) . '/assets/css/frontend.css' );
	}

	// trigered on deactivation of the plugin (called only once)
	public function upr_plugin_deactivate() {
		// flush permalinks
		// drop options
		/*
		delete_option('upr_prayer_list_title');
		delete_option('upr_no_prayer_per_page');
		delete_option('upr_login_not_required_request');
		delete_option('upr_prayer_login_required_prayer');
		delete_option('upr_prayer_send_email');

		delete_option('upr_prayer_send_admin_email');
		delete_option('upr_prayer_default_status_pending');
		delete_option('upr_hide_prayer_button');
		delete_option('upr_hide_prayer_count');
		delete_option('upr_display_username_on_prayer_listing');
		delete_option('upr_prayer_hide_captcha');
		delete_option('upr_prayer_spam_prevention');
		delete_option('upr_prayer_spam_DDoS_prevention');
		delete_option('upr_prayer_show_country');
		delete_option('upr_show_prayer_category');
		delete_option('upr_pray_prayed_button_ip');
		delete_option('upr_time_interval_pray_prayed_button');
		delete_option('upr_prayer_page_slug');
		*/
		flush_rewrite_rules();
	}
}

function upr_pray_email_settings() {
	include plugin_dir_path( __FILE__ ) . 'inc/upr_email_settings.php';
}

function upr_pray_settings() {
	include plugin_dir_path( __FILE__ ) . 'inc/upr_settings.php';
}

function upr_pray_statistic() {
	include plugin_dir_path( __FILE__ ) . 'inc/upr_pray_statistic.php';
}

function upr_prayer_sub_menu() {
	add_submenu_page(
		'edit.php?post_type=prayers',
		__( 'Email Settings', 'prayers' ),
		__( 'Email Settings', 'prayers' ),
		'manage_options',
		'pray-email-settings',
		'upr_pray_email_settings'
	);
	add_submenu_page(
		'edit.php?post_type=prayers',
		__( 'Settings', 'prayers' ),
		__( 'Settings', 'prayers' ),
		'manage_options',
		'pray-settings',
		'upr_pray_settings'
	);
	add_submenu_page(
		'edit.php?post_type=prayers',
		__( 'Statistic', 'prayers' ),
		__( 'Statistic', 'prayers' ),
		'publish_pages',
		'pray-statistic',
		'upr_pray_statistic'
	);
}

// initialize
global $userprayersrequests, $prayers_shortcode;
$userprayersrequests = new user_prayers_request();
$prayers_shortcode   = new upr_shortcode();
?>
