<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

if ( isset( $_REQUEST['captcha_code'] ) ) {
    session_start();
}

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Nope, not accessing this' );
} // Exit if accessed directly

include_once ( 'upr_post_exists.php' );

class upr_shortcode {

	private $select_country = array(
		'US' => 'United States',
		'AF' => 'Afghanistan',
		'AL' => 'Albania',
		'DZ' => 'Algeria',
		'AS' => 'American Samoa',
		'AD' => 'Andorra',
		'AO' => 'Angola',
		'AI' => 'Anguilla',
		'AQ' => 'Antarctica',
		'AG' => 'Antigua and Barbuda',
		'AR' => 'Argentina',
		'AM' => 'Armenia',
		'AW' => 'Aruba',
		'AU' => 'Australia',
		'AT' => 'Austria',
		'AZ' => 'Azerbaijan',
		'BS' => 'Bahamas',
		'BH' => 'Bahrain',
		'BD' => 'Bangladesh',
		'BB' => 'Barbados',
		'BY' => 'Belarus',
		'BE' => 'Belgium',
		'BZ' => 'Belize',
		'BJ' => 'Benin',
		'BM' => 'Bermuda',
		'BT' => 'Bhutan',
		'BO' => 'Bolivia',
		'BQ' => 'Bonaire',
		'BA' => 'Bosnia and Herzegovina',
		'BW' => 'Botswana',
		'BV' => 'Bouvet Island',
		'BR' => 'Brazil',
		'IO' => 'British Indian Ocean Territory',
		'BN' => 'Brunei Darussalam',
		'BG' => 'Bulgaria',
		'BF' => 'Burkina Faso',
		'BI' => 'Burundi',
		'KH' => 'Cambodia',
		'CM' => 'Cameroon',
		'CA' => 'Canada',
		'CV' => 'Cape Verde',
		'KY' => 'Cayman Islands',
		'CF' => 'Central African Republic',
		'TD' => 'Chad',
		'CL' => 'Chile',
		'CN' => 'China',
		'CX' => 'Christmas Island',
		'CC' => 'Cocos (Keeling) Islands',
		'CO' => 'Colombia',
		'KM' => 'Comoros',
		'CG' => 'Congo',
		'CD' => 'Democratic Republic of the Congo',
		'CK' => 'Cook Islands',
		'CR' => 'Costa Rica',
		'HR' => 'Croatia',
		'CU' => 'Cuba',
		'CW' => 'Curacao',
		'CY' => 'Cyprus',
		'CZ' => 'Czech Republic',
		'CI' => "Cote d'Ivoire",
		'DK' => 'Denmark',
		'DJ' => 'Djibouti',
		'DM' => 'Dominica',
		'DO' => 'Dominican Republic',
		'EC' => 'Ecuador',
		'EG' => 'Egypt',
		'SV' => 'El Salvador',
		'GQ' => 'Equatorial Guinea',
		'ER' => 'Eritrea',
		'EE' => 'Estonia',
		'ET' => 'Ethiopia',
		'FK' => 'Falkland Islands (Malvinas)',
		'FO' => 'Faroe Islands',
		'FJ' => 'Fiji',
		'FI' => 'Finland',
		'FR' => 'France',
		'GF' => 'French Guiana',
		'PF' => 'French Polynesia',
		'TF' => 'French Southern Territories',
		'GA' => 'Gabon',
		'GM' => 'Gambia',
		'GE' => 'Georgia',
		'DE' => 'Germany',
		'GH' => 'Ghana',
		'GI' => 'Gibraltar',
		'GR' => 'Greece',
		'GL' => 'Greenland',
		'GD' => 'Grenada',
		'GP' => 'Guadeloupe',
		'GU' => 'Guam',
		'GT' => 'Guatemala',
		'GG' => 'Guernsey',
		'GN' => 'Guinea',
		'GW' => 'Guinea-Bissau',
		'GY' => 'Guyana',
		'HT' => 'Haiti',
		'HM' => 'Heard Island and McDonald Islands',
		'VA' => 'Holy See (Vatican City State)',
		'HN' => 'Honduras',
		'HK' => 'Hong Kong',
		'HU' => 'Hungary',
		'IS' => 'Iceland',
		'IN' => 'India',
		'ID' => 'Indonesia',
		'IR' => 'Iran, Islamic Republic of',
		'IQ' => 'Iraq',
		'IE' => 'Ireland',
		'IM' => 'Isle of Man',
		'IL' => 'Israel',
		'IT' => 'Italy',
		'JM' => 'Jamaica',
		'JP' => 'Japan',
		'JE' => 'Jersey',
		'JO' => 'Jordan',
		'KZ' => 'Kazakhstan',
		'KE' => 'Kenya',
		'KI' => 'Kiribati',
		'KP' => "Korea, Democratic People's Republic of",
		'KR' => 'Korea, Republic of',
		'KW' => 'Kuwait',
		'KG' => 'Kyrgyzstan',
		'LA' => "Lao People's Democratic Republic",
		'LV' => 'Latvia',
		'LB' => 'Lebanon',
		'LS' => 'Lesotho',
		'LR' => 'Liberia',
		'LY' => 'Libya',
		'LI' => 'Liechtenstein',
		'LT' => 'Lithuania',
		'LU' => 'Luxembourg',
		'MO' => 'Macao',
		'MK' => 'Macedonia, the Former Yugoslav Republic of',
		'MG' => 'Madagascar',
		'MW' => 'Malawi',
		'MY' => 'Malaysia',
		'MV' => 'Maldives',
		'ML' => 'Mali',
		'MT' => 'Malta',
		'MH' => 'Marshall Islands',
		'MQ' => 'Martinique',
		'MR' => 'Mauritania',
		'MU' => 'Mauritius',
		'YT' => 'Mayotte',
		'MX' => 'Mexico',
		'FM' => 'Micronesia, Federated States of',
		'MD' => 'Moldova, Republic of',
		'MC' => 'Monaco',
		'MN' => 'Mongolia',
		'ME' => 'Montenegro',
		'MS' => 'Montserrat',
		'MA' => 'Morocco',
		'MZ' => 'Mozambique',
		'MM' => 'Myanmar',
		'NA' => 'Namibia',
		'NR' => 'Nauru',
		'NP' => 'Nepal',
		'NL' => 'Netherlands',
		'NC' => 'New Caledonia',
		'NZ' => 'New Zealand',
		'NI' => 'Nicaragua',
		'NE' => 'Niger',
		'NG' => 'Nigeria',
		'NU' => 'Niue',
		'NF' => 'Norfolk Island',
		'MP' => 'Northern Mariana Islands',
		'NO' => 'Norway',
		'OM' => 'Oman',
		'PK' => 'Pakistan',
		'PW' => 'Palau',
		'PS' => 'Palestine, State of',
		'PA' => 'Panama',
		'PG' => 'Papua New Guinea',
		'PY' => 'Paraguay',
		'PE' => 'Peru',
		'PH' => 'Philippines',
		'PN' => 'Pitcairn',
		'PL' => 'Poland',
		'PT' => 'Portugal',
		'PR' => 'Puerto Rico',
		'QA' => 'Qatar',
		'RO' => 'Romania',
		'RU' => 'Russian Federation',
		'RW' => 'Rwanda',
		'RE' => 'Reunion',
		'BL' => 'Saint Barthelemy',
		'SH' => 'Saint Helena',
		'KN' => 'Saint Kitts and Nevis',
		'LC' => 'Saint Lucia',
		'MF' => 'Saint Martin (French part)',
		'PM' => 'Saint Pierre and Miquelon',
		'VC' => 'Saint Vincent and the Grenadines',
		'WS' => 'Samoa',
		'SM' => 'San Marino',
		'ST' => 'Sao Tome and Principe',
		'SA' => 'Saudi Arabia',
		'SN' => 'Senegal',
		'RS' => 'Serbia',
		'SC' => 'Seychelles',
		'SL' => 'Sierra Leone',
		'SG' => 'Singapore',
		'SX' => 'Sint Maarten (Dutch part)',
		'SK' => 'Slovakia',
		'SI' => 'Slovenia',
		'SB' => 'Solomon Islands',
		'SO' => 'Somalia',
		'ZA' => 'South Africa',
		'GS' => 'South Georgia and the South Sandwich Islands',
		'SS' => 'South Sudan',
		'ES' => 'Spain',
		'LK' => 'Sri Lanka',
		'SD' => 'Sudan',
		'SR' => 'Suriname',
		'SJ' => 'Svalbard and Jan Mayen',
		'SZ' => 'Swaziland',
		'SE' => 'Sweden',
		'CH' => 'Switzerland',
		'SY' => 'Syrian Arab Republic',
		'TW' => 'Taiwan',
		'TJ' => 'Tajikistan',
		'TZ' => 'United Republic of Tanzania',
		'TH' => 'Thailand',
		'TL' => 'Timor-Leste',
		'TG' => 'Togo',
		'TK' => 'Tokelau',
		'TO' => 'Tonga',
		'TT' => 'Trinidad and Tobago',
		'TN' => 'Tunisia',
		'TR' => 'Turkey',
		'TM' => 'Turkmenistan',
		'TC' => 'Turks and Caicos Islands',
		'TV' => 'Tuvalu',
		'UG' => 'Uganda',
		'UA' => 'Ukraine',
		'AE' => 'United Arab Emirates',
		'GB' => 'United Kingdom',
		'UM' => 'United States Minor Outlying Islands',
		'UY' => 'Uruguay',
		'UZ' => 'Uzbekistan',
		'VU' => 'Vanuatu',
		'VE' => 'Venezuela',
		'VN' => 'Viet Nam',
		'VG' => 'British Virgin Islands',
		'VI' => 'US Virgin Islands',
		'WF' => 'Wallis and Futuna',
		'EH' => 'Western Sahara',
		'YE' => 'Yemen',
		'ZM' => 'Zambia',
		'ZW' => 'Zimbabwe',
	);

	// on initialize
	public function __construct() {
		if ( ! is_admin() ) { // Don't register if it's admin panel
			add_action( 'init', array( $this, 'register_upr_shortcodes' ) );
		} //shortcodes
	}

	public function register_upr_shortcodes() {
		add_shortcode( 'upr_prophecy_form', array( $this, 'upr_prophecy_form_shortcode_output' ) );
		add_shortcode( 'upr_list_prophecies', array( $this, 'upr_list_prophecies_shortcode_output' ) );

		add_shortcode( 'upr_form', array( $this, 'upr_form_shortcode_output' ) );		
        add_shortcode( 'upr_list_prayers', array( $this, 'upr_list_prayers_shortcode_output' ) );
		add_shortcode( 'upr_list_prayers_lowest_pray_count', array( $this, 'upr_list_prayers_lowest_pray_count_shortcode_output' ) );
		add_shortcode( 'upr_list_prayer_next', array( $this, 'upr_list_prayer_next_shortcode_output' ) );

		add_shortcode( 'upr_witness_form', array( $this, 'upr_witness_form_shortcode_output' ) );
		add_shortcode( 'upr_list_witnesses', array( $this, 'upr_list_witnesses_shortcode_output' ) );
		add_shortcode( 'upr_list_all', array( $this, 'upr_list_all_shortcode_output' ) );
		add_shortcode( 'upr_list_all_lowest', array( $this, 'upr_list_all_lowest_shortcode_output' ) );
		add_shortcode( 'upr_form_all', array( $this, 'upr_form_all_shortcode_output' ) );
	}

	public function upr_website_email( $sender ) {
		$prayer_email_user = get_option( 'prayer_email_user' );
		$sitename          = strtolower( $_SERVER['SERVER_NAME'] );
		if ( substr( $sitename, 0, 4 ) == 'www.' ) {
			$sitename = substr( $sitename, 4 );
		}
		$illegal_chars_username = array( '(', ')', '<', '>', ',', ';', ':', '\\', '"', '[', ']', '@', "'", ' ' );
		$username               = str_replace( $illegal_chars_username, '', get_option( 'blogname' ) );
		$sender_emailuser       = ( isset( $prayer_email_user ) and ! empty( $prayer_email_user ) ) ? $prayer_email_user : $username . '@' . $sitename;
		$sender_email           = $sender_emailuser;

		return $sender_email;
	}

	public function upr_website_name( $name ) {
		// $prayer_email_from = unserialize(get_option('prayer_email_from'));
		$prayer_email_from = get_option( 'prayer_email_from' );
		$site_name         = ( isset( $prayer_email_from ) and ! empty( $prayer_email_from ) ) ? $prayer_email_from : get_option( 'blogname' );

		return $site_name;
	}

	function custom_mails( $args ) {
		$copy_to = get_option( 'prayer_admin_email_cc' );

		$copy_to = explode( ',', $copy_to );
		foreach ( $copy_to as $cc_email ) {
			if ( ! empty( $cc_email ) ) {
				if ( is_array( $args['headers'] ) ) {
					$args['headers'][] = 'cc: ' . $cc_email;
				} else {
					$args['headers'] .= 'cc: ' . $cc_email . "\r\n";
				}
			}
		}
		return $args;
	}

	public function upr_prophecy_form_shortcode_output() {
        return $this->upr_form_shortcode_output_generic( 'prophecies' );
    }
	public function upr_witness_form_shortcode_output() {
        return $this->upr_form_shortcode_output_generic( 'witnesses' );
    }
	public function upr_form_shortcode_output() {
        return $this->upr_form_shortcode_output_generic( 'prayers' );
    }

	public function upr_form_shortcode_output_generic( $type ) {
		$upr_prayer_hide_captcha = get_option( 'upr_prayer_hide_captcha' );
		if ( ( isset( $_REQUEST['g-recaptcha-response'] ) && ! empty( $_REQUEST['g-recaptcha-response'] ) ) || $upr_prayer_hide_captcha != 1 ) {
			// your site secret key
			if ( $upr_prayer_hide_captcha == 1 ) {
				$secret = get_option( 'upr_prayer_secret' );

				$url  = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_REQUEST['g-recaptcha-response'];
				$curl = curl_init();
				curl_setopt( $curl, CURLOPT_URL, $url );
				curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $curl, CURLOPT_TIMEOUT, 15 );
				curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, true );
				curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, true );
				$curlData = curl_exec( $curl );

				curl_close( $curl );

				$responseData = json_decode( $curlData, true );
			} else {
				$responseData['success'] = true;
			}
			if ( $responseData['success'] != 'true' ) {
				 // $message = '<div class="col-md-11 alert-success" style="color:#F00">Sorry, the re-Captcha security key could not be verified. Please enter a valid security key.</div>';

			} else {
                $request = '';
                if ( $type == 'prophecies' ) {
                    $request = 'save_prophecy_data';
                } elseif ( $type == 'witnesses' ) {
                    $request = 'save_witnesses_data';
                } elseif ( $type == 'prayers' ) {
                    $request = 'save_prayer_data';
                }
				if ( isset( $_REQUEST[$request] ) ) {
					// Create post object
					$message = '';
					// spam prevent
					if ( isset( $_REQUEST['is_humans'] ) ) {
						if ( $_REQUEST['is_humans'] != '' ) {
							$message = '<div class="col-md-11 alert-success" style="color:#F00">It is spam</div>';
						}
					}

					if ( isset( $_REQUEST['captcha_code'] ) ) {
						if ( $_REQUEST['captcha_code'] != $_SESSION['captcha_code'] ) {
							$message = '<div class="col-md-11 alert-success" style="color:#F00">Please enter correct captcha value.</div>';
						}
					}

					$missingInput = false;
					if ( empty( $_REQUEST['prayer_messages'] )
						|| empty( $_REQUEST['prayer_title'] )
						|| empty( $_REQUEST['prayer_author_name'] )
					) {
						$missingInput = true;
						echo '<div class="col-md-11 alert-danger">' . __( 'Please enter a author, title and message.', 'prayers' ) . '</div>';
					}

					if ( empty( $message ) && ! $missingInput ) {
						$post_status                       = 'publish';
						$upr_prayer_default_status_pending = get_option( 'upr_prayer_default_status_pending' );
						if ( $upr_prayer_default_status_pending == 1 ) {
							$post_status = 'pending';
						}

						$upr_prayer_send_email = get_option( 'upr_prayer_send_email' );
                        $_REQUEST['prayer_title'] = strip_tags( $_REQUEST['prayer_title'] );
                        $prayer_messages = strip_tags( $_REQUEST['prayer_messages'], '<br>' );
                        $prayer_post           = array(
                            'post_title'   => $_REQUEST['prayer_title'],
                            'post_content' => $prayer_messages,
                            'post_type'    => $type,
                            'post_status'  => $post_status,
                        );
                        // check if X already exists
                        $post_exists = upr_post_exists(
                            $prayer_post['post_title'],
                            $prayer_post['post_content'],
                            '',
                            $type
                        );
                        if ($post_exists > 0) {
                            $message = '<div class="col-md-11 alert-danger">'
								. __( 'A item with the same text already exists. So it is not saved!', 'prayers' )
								. '</div>';
                        }
                                                
						// Insert the post into the database
                        if ($post_exists == 0) {
                            $prayer_id                   = wp_insert_post( $prayer_post );
                            $praylink                    = get_permalink( $prayer_id );
                        }
                        $upr_prayer_send_email       = get_option( 'upr_prayer_send_email' );
						$upr_prayer_send_admin_email = get_option( 'upr_prayer_send_admin_email' );
						if ( $prayer_id ) {
							if ( isset( $_REQUEST['prayer_author_category'] ) ) {
								$category = $_REQUEST['prayer_author_category'];
								wp_set_post_terms( $prayer_id, array( $category ), 'prayertypes', false );
							}
							update_post_meta( $prayer_id, 'prayers_name', wp_strip_all_tags( $_REQUEST['prayer_author_name'] ) );
							update_post_meta( $prayer_id, 'prayers_email', wp_strip_all_tags( $_REQUEST['prayer_author_email'] ) );
							if ( isset( $_REQUEST['prayer_author_country'] ) ) {
								update_post_meta( $prayer_id, 'prayers_country', $_REQUEST['prayer_author_country'] );
							}
							$to = $_REQUEST['prayer_author_email'];

							$prayer_email_from = get_option( 'prayer_email_from' );
							$prayer_email_user = get_option( 'prayer_email_user' );

							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

							if ( $upr_prayer_send_email == 1 ) {
								if ( ! empty( $_REQUEST['prayer_author_email'] ) ) {
									add_filter( 'wp_mail_from', array( $this, 'upr_website_email' ) );
									add_filter( 'wp_mail_from_name', array( $this, 'upr_website_name' ) );
									$body                     = '';
									$prayer_email_req_subject = get_option( 'prayer_email_req_subject' );
									if ( ! empty( $prayer_email_req_subject ) ) {
										$subject = $prayer_email_req_subject;
									} else {
										$subject = 'Prayer request confirmation';
									}
									$prayer_email_req_messages = get_option( 'prayer_email_req_messages' );
									if ( ! empty( $prayer_email_req_messages ) ) {
										$body = $prayer_email_req_messages;
										$body = str_replace(
											array( '{prayer_author_name}' ),
											array( $_REQUEST['prayer_author_name'] ),
											$body
										);
									} else {
										// todo translation!
										$body  = 'Hello ' . $_REQUEST['prayer_author_name'] . ', <br> <p>Thank you for submitting your prayer request.<br />';
										$body .= 'We welcome all requests and we delight in lifting you and your requests up to God in prayer.<br />';
										$body .= 'God Bless you, and remember, God knows the prayers that are coming and hears them even before they are spoken.<br />';
										$body .= '<a href="' . $praylink . '">' . $praylink . '</a>';
										$body .= '<br /><br />Blessings,<br/ >Prayer Team</p>';
									}
									wp_mail( $to, $subject, $body, $headers );
								}
							}

							if ( $upr_prayer_send_admin_email == 1 ) {
								$upr_prayer_req_admin_email = get_option( 'prayer_req_admin_email' );

								if ( isset( $upr_prayer_req_admin_email ) && $upr_prayer_req_admin_email != '' ) {
									$to_admin = $upr_prayer_req_admin_email;
								} else {
									$to_admin = get_option( 'admin_email' );
								}
								if ( ! empty( $to_admin ) ) {
									add_filter( 'wp_mail_from', array( $this, 'upr_website_email' ) );
									add_filter( 'wp_mail_from_name', array( $this, 'upr_website_name' ) );
									add_filter( 'wp_mail', array( $this, 'custom_mails' ), 10, 1 );
									$body                       = '';
									$prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
									if ( ! empty( $prayer_email_admin_subject ) ) {
										$subject = $prayer_email_admin_subject;
									} else {
										$subject = 'New {request_type} received';
									}
									$subject = str_replace( array( '{request_type}' ), array( 'Prayer Request' ), $subject );
									// todo:: $data array missing
									// $prayer_author_info = get_userdata($data['prayer_author']);
									// $to = $prayer_author_info->user_email;
									$prayer_email_admin_messages = get_option( 'prayer_email_admin_messages' );

									if ( ! empty( $prayer_email_admin_messages ) ) {
										$body = $prayer_email_admin_messages;
										$body = str_replace(
											array(
												'{prayer_author_name}',
												'{prayer_author_email}',
												'{prayer_title}',
												'{prayer_messages}',
												'{prayer_author_info}',
												'{request_type}',
											),
											array(
												$_REQUEST['prayer_author_name'],
												$_REQUEST['prayer_author_email'],
												$_REQUEST['prayer_title'],
												$prayer_messages,
												$_REQUEST['prayer_author_name'],
												'Prayer Request',
											),
											$body
										);
									} else {
                                        if ( $type == 'prophecies' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Prophecy Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            // todo translation!
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'prophecy to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        } elseif ( $type == 'prayers' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Prayer Request Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'prayer request';
                                            $body    .= ' to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        } elseif ( $type == 'witnesses' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Witness Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'witness';
                                            $body    .= ' to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        }
									}
									wp_mail( $to_admin, $subject, $body, $headers );
								}
							}
                            
							$message = '<div class="col-md-11 alert-success">';
                            if ( $type == 'prophecies' ) {
                                $message .= __( 'Thank you. Your prophecy has been received, it will be published soon.', 'prayers' );
                            } elseif ( $type == 'prayers' ) {
                                $message .= __( 'Thank you. Your prayer request has been received, it will be published soon.', 'prayers' );
                            } elseif ( $type == 'witnesses' ) {
                                $message .= __( 'Thank you. Your witness has been received, it will be published soon.', 'prayers' );
                            }                          
                            
							$message .= '</div>';
						}
					}
					unset( $_REQUEST );
				}
			}
		} else {
			// $message = '<div class="col-md-11 alert-success" style="color:#F00">Invalid captcha</div>';
		}
		ob_start();
        if (file_exists(__DIR__.'/../view/submit_form.php')) {
            include __DIR__.'/../view/submit_form.php';
        } else {
            include __DIR__.'/../view/submit_form.dist.php';
        }
        return ob_get_clean();
	}
    
    public function upr_form_all_shortcode_output() {
		$upr_prayer_hide_captcha = get_option( 'upr_prayer_hide_captcha' );
		if ( ( isset( $_REQUEST['g-recaptcha-response'] ) && ! empty( $_REQUEST['g-recaptcha-response'] ) ) || $upr_prayer_hide_captcha != 1 ) {
			// your site secret key
			if ( $upr_prayer_hide_captcha == 1 ) {
				$secret = get_option( 'upr_prayer_secret' );

				$url  = 'https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_REQUEST['g-recaptcha-response'];
				$curl = curl_init();
				curl_setopt( $curl, CURLOPT_URL, $url );
				curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $curl, CURLOPT_TIMEOUT, 15 );
				curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, true );
				curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, true );
				$curlData = curl_exec( $curl );

				curl_close( $curl );

				$responseData = json_decode( $curlData, true );
			} else {
				$responseData['success'] = true;
			}
			if ( $responseData['success'] != 'true' ) {
				 // $message = '<div class="col-md-11 alert-success" style="color:#F00">Sorry, the re-Captcha security key could not be verified. Please enter a valid security key.</div>';

			} else {
				if ( isset( $_REQUEST['save_data'] ) ) {
                    $type = $_REQUEST['save_data'];
                    if (!in_array($type, ['prophecies', 'witnesses', 'prayers'])) {
                        die ('invalid type');
                    }
					// Create post object
					$message = '';
					// spam prevent
					if ( isset( $_REQUEST['is_humans'] ) ) {
						if ( $_REQUEST['is_humans'] != '' ) {
							$message = '<div class="col-md-11 alert-success" style="color:#F00">It is spam</div>';
						}
					}

					if ( isset( $_REQUEST['captcha_code'] ) ) {
						if ( $_REQUEST['captcha_code'] != $_SESSION['captcha_code'] ) {
							$message = '<div class="col-md-11 alert-success" style="color:#F00">Please enter correct captcha value.</div>';
						}
					}

					$missingInput = false;
					if ( empty( $_REQUEST['prayer_messages'] )
						|| empty( $_REQUEST['prayer_title'] )
						|| empty( $_REQUEST['prayer_author_name'] )
					) {
						$missingInput = true;
						echo '<div class="col-md-11 alert-danger">' . __( 'Please enter a author, title and message.', 'prayers' ) . '</div>';
					}
                    
                    if ( get_option( 'upr_accept_privacy' ) != '' 
                        && $_REQUEST['upr_acceptprivacy'] != 1 ) {
                        $missingInput = true;
						echo '<div class="col-md-11 alert-danger">' . __( 'Please accept the privacy conditions!', 'prayers' ) . '</div>';    
                    }

					if ( empty( $message ) && ! $missingInput ) {
						$post_status                       = 'publish';
						$upr_prayer_default_status_pending = get_option( 'upr_prayer_default_status_pending' );
						if ( $upr_prayer_default_status_pending == 1 ) {
							$post_status = 'pending';
						}

                        $upr_prayer_send_email = get_option( 'upr_prayer_send_email' );
                        $_REQUEST['prayer_title'] = strip_tags( $_REQUEST['prayer_title'] ); // todo fix this
                        //$prayer_messages = strip_tags( $_REQUEST['prayer_messages'] , '<br>' );
                        $prayer_messages = filter_input(INPUT_POST, 'prayer_messages', FILTER_SANITIZE_STRING);
                        $prayer_post           = array(
                            'post_title'   => $_REQUEST['prayer_title'],
                            'post_content' => $prayer_messages,
                            'post_type'    => $type,
                            'post_status'  => $post_status,
                        );
                        // check if X already exists
                        $post_exists = upr_post_exists(
                            $prayer_post['post_title'],
                            $prayer_post['post_content'],
                            '',
                            $type
                        );
                        if ($post_exists > 0) {
                            $message = '<div class="col-md-11 alert-danger">'
								. __( 'A item with the same text already exists. So it is not saved!', 'prayers' )
								. '</div>';
                        }
                                                
						// Insert the post into the database
                        if ($post_exists == 0) {
                            $prayer_id                   = wp_insert_post( $prayer_post );
                            $praylink                    = get_permalink( $prayer_id );
                        }
                        $upr_prayer_send_email       = get_option( 'upr_prayer_send_email' );
						$upr_prayer_send_admin_email = get_option( 'upr_prayer_send_admin_email' );
						if ( $prayer_id ) {
							if ( isset( $_REQUEST['prayer_author_category'] ) ) {
								$category = $_REQUEST['prayer_author_category'];
								wp_set_post_terms( $prayer_id, array( $category ), 'prayertypes', false );
							}
							update_post_meta( $prayer_id, 'prayers_name', wp_strip_all_tags( $_REQUEST['prayer_author_name'] ) );
							update_post_meta( $prayer_id, 'prayers_email', wp_strip_all_tags( $_REQUEST['prayer_author_email'] ) );
							if ( isset( $_REQUEST['prayer_author_country'] ) ) {
								update_post_meta( $prayer_id, 'prayers_country', $_REQUEST['prayer_author_country'] );
							}
							$to = $_REQUEST['prayer_author_email'];

							$prayer_email_from = get_option( 'prayer_email_from' );
							$prayer_email_user = get_option( 'prayer_email_user' );

							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-Type: text/html; charset=UTF-8' . "\r\n";

							if ( $upr_prayer_send_email == 1 ) {
								if ( ! empty( $_REQUEST['prayer_author_email'] ) ) {
									add_filter( 'wp_mail_from', array( $this, 'upr_website_email' ) );
									add_filter( 'wp_mail_from_name', array( $this, 'upr_website_name' ) );
									$body                     = '';
									$prayer_email_req_subject = get_option( 'prayer_email_req_subject' );
									if ( ! empty( $prayer_email_req_subject ) ) {
										$subject = $prayer_email_req_subject;
									} else {
										$subject = 'Prayer request confirmation';
									}
									$prayer_email_req_messages = get_option( 'prayer_email_req_messages' );
									if ( ! empty( $prayer_email_req_messages ) ) {
										$body = $prayer_email_req_messages;
										$body = str_replace(
											array( '{prayer_author_name}' ),
											array( $_REQUEST['prayer_author_name'] ),
											$body
										);
									} else {
										// todo translation!
										$body  = 'Hello ' . $_REQUEST['prayer_author_name'] . ', <br> <p>Thank you for submitting your prayer request.<br />';
										$body .= 'We welcome all requests and we delight in lifting you and your requests up to God in prayer.<br />';
										$body .= 'God Bless you, and remember, God knows the prayers that are coming and hears them even before they are spoken.<br />';
										$body .= '<a href="' . $praylink . '">' . $praylink . '</a>';
										$body .= '<br /><br />Blessings,<br/ >Prayer Team</p>';
									}
									wp_mail( $to, $subject, $body, $headers );
								}
							}

							if ( $upr_prayer_send_admin_email == 1 ) {
								$upr_prayer_req_admin_email = get_option( 'prayer_req_admin_email' );

								if ( isset( $upr_prayer_req_admin_email ) && $upr_prayer_req_admin_email != '' ) {
									$to_admin = $upr_prayer_req_admin_email;
								} else {
									$to_admin = get_option( 'admin_email' );
								}
								if ( ! empty( $to_admin ) ) {
									add_filter( 'wp_mail_from', array( $this, 'upr_website_email' ) );
									add_filter( 'wp_mail_from_name', array( $this, 'upr_website_name' ) );
									add_filter( 'wp_mail', array( $this, 'custom_mails' ), 10, 1 );
									$body                       = '';
									$prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
									if ( ! empty( $prayer_email_admin_subject ) ) {
										$subject = $prayer_email_admin_subject;
									} else {
										$subject = 'New {request_type} received';
									}
									$subject = str_replace( array( '{request_type}' ), array( 'Prayer Request' ), $subject );
									// todo:: $data array missing
									// $prayer_author_info = get_userdata($data['prayer_author']);
									// $to = $prayer_author_info->user_email;
									$prayer_email_admin_messages = get_option( 'prayer_email_admin_messages' );

									if ( ! empty( $prayer_email_admin_messages ) ) {
										$body = $prayer_email_admin_messages;
										$body = str_replace(
											array(
												'{prayer_author_name}',
												'{prayer_author_email}',
												'{prayer_title}',
												'{prayer_messages}',
												'{prayer_author_info}',
												'{request_type}',
											),
											array(
												$_REQUEST['prayer_author_name'],
												$_REQUEST['prayer_author_email'],
												$_REQUEST['prayer_title'],
												$prayer_messages,
												$_REQUEST['prayer_author_name'],
												'Prayer Request',
											),
											$body
										);
									} else {
                                        if ( $type == 'prophecies' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Prophecy Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            // todo translation!
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'prophecy to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        } elseif ( $type == 'prayers' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Prayer Request Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'prayer request';
                                            $body    .= ' to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        } elseif ( $type == 'witnesses' ) {
                                            $prayer_email_admin_subject = get_option( 'prayer_email_admin_subject' );
                                            if ( ! empty( $prayer_email_admin_subject ) ) {
                                                $subject = $prayer_email_admin_subject;
                                            } else {
                                                $subject = 'New Witness Received To Moderate';
                                            }
                                            // $headers  .= 'MIME-Version: 1.0' . "\r\n";
                                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                                            $body     = 'Hello, <br> <p>You have received a new ';
                                            $body    .= 'witness';
                                            $body    .= ' to moderate with following details:</p>';
                                            $body    .= '<b>Name:</b> ' . $_REQUEST['prayer_author_name'] . '<br>';
                                            // todo:: $data array missing
                                            // if($data['prayer_author_email']!='')
                                            // $body .= '<b>Email:</b> '.$_REQUEST['prayer_author_email'].'<br>';
                                            $body .= '<b>Title:</b> ' . $_REQUEST['prayer_title'] . '<br>';
                                            $body .= '<b>Message:</b> ' . $prayer_messages . '<br>';
                                            $body .= '<a href="' . $praylink . '">' . $praylink . '</a><br>';
                                            $body .= '<br>Thank you';
                                        }
									}
									wp_mail( $to_admin, $subject, $body, $headers );
								}
							}
                            
							$message = '<br><div class="col-md-11 alert-success">';
                            if ( $type == 'prophecies' ) {
                                $message .= __( 'Thank you. Your impression has been received, it will be published soon.', 'prayers' );
                            } elseif ( $type == 'prayers' ) {
                                $message .= __( 'Thank you. Your prayer request has been received, it will be published soon.', 'prayers' );
                            } elseif ( $type == 'witnesses' ) {
                                $message .= __( 'Thank you. Your hearing has been received, it will be published soon.', 'prayers' );
                            }                          
                            
							$message .= '</div>';
						}
					}
					unset( $_REQUEST );
				}
			}
		} else {
			// $message = '<div class="col-md-11 alert-success" style="color:#F00">Invalid captcha</div>';
		}
		ob_start();
        if (file_exists(__DIR__.'/../view/submit_all_form.php')) {
            include __DIR__.'/../view/submit_all_form.php';
        } else {
            include __DIR__.'/../view/submit_all_form.dist.php';
        }
        return ob_get_clean();
	} // all form

	public function upr_list_prayer_next_shortcode_output( $atts, $content, $tag ) {
		$html = upr_get_prayers_next_output();

		return $html;
	}
    
	public function upr_list_prayers_lowest_pray_count_shortcode_output( $atts, $content, $tag ) {
		$html = upr_get_prayers_lowest_pray_count_output();

		return $html;
	}
    
	public function upr_list_prophecies_shortcode_output( $atts, $content, $tag ) {
        // todo arguments is not used
		// build default arguments
		$arguments = shortcode_atts(
			array(
				'prayer_id'         => '',
				'number_of_prayers' => -1,
			),
			$atts,
			$tag
		);

		$html = upr_get_prophecies_output( $arguments );

		return $html;
	}

	public function upr_list_witnesses_shortcode_output( $atts, $content, $tag ) {
		$html = upr_get_witnesses_output();

		return $html;
	}

    
	public function upr_list_all_lowest_shortcode_output( $atts, $content, $tag ) {
		$html = upr_list_all_output('lowest');

		return $html; 
    }

	public function upr_list_all_shortcode_output( $atts, $content, $tag ) {
		$html = upr_list_all_output();

		return $html; 
    }
	// --

	public function upr_list_prayers_shortcode_output( $atts, $content, $tag ) {
		// build default arguments
		$arguments = shortcode_atts(
			array(
				'prayer_id'         => '',
				'number_of_prayers' => -1,
			),
			$atts,
			$tag
		);

		// uses the main output function of the location class
		$html = upr_get_prayers_output( $arguments );

		return $html;
	}
}

function upr_comments( $comment, $args, $depth ) {
    if (file_exists(__DIR__.'/../view/comment_form.php')) {
        include __DIR__.'/../view/comment_form.php';
    } else {
        include __DIR__.'/../view/comment_form.dist.php';
    }
}

function upr_all_comments( $comment, $args, $depth ) {
    if (file_exists(__DIR__.'/../view/comment_all_form.php')) {
        include __DIR__.'/../view/comment_all_form.php';
    } else {
        include __DIR__.'/../view/comment_all_form.dist.php';
    }
}

function upr_update_cookie() {
	global $wpdb;
	$upr_prayer_page_slug = get_option( 'upr_prayer_page_slug' );
    if ( get_option( 'upr_disable_hide_all_prayed' ) == 1 ) {
        setcookie( 'prayer_requests', null, -1, '/' ); // 30days
        return;
    }
	// execute only if cookie is set and on the prayers page
	if (
		! isset( $_COOKIE['prayer_requests'] )
		|| (
			! empty( $upr_prayer_page_slug )
			&& strpos( $_SERVER['REQUEST_URI'], $upr_prayer_page_slug ) === false
		)
	) {
		return;
	}
	// get all prayers ids
	$query                     = '
        SELECT ID
        FROM ' . $wpdb->posts . ' p
        WHERE (post_type = "prayers" OR post_type = "prophecies" OR post_type = "witnesses")
        AND post_status = "publish"
    ';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$query .= ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
	}
	$queryResult = $wpdb->get_results( $query );
	$prids       = array();
	foreach ( $queryResult as $qr ) {
		$prids[ $qr->ID ] = true;
	}
	// unset all not anymore existing ids in the cookie array
    $prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
    if ( ! is_array( $prayer_requests ) ) {
        $prayer_requests = array();
    }
	foreach ( $prayer_requests as $id => $cnt ) {
		if ( ! array_key_exists( $id, $prids ) ) {
			unset( $prayer_requests[ $id ] );
		}
	}

    $cookiedata = json_encode( $prayer_requests );
    if ( strlen($cookiedata) > 4000 ) {
        // then reset the cookie array
        $cookiedata = array();
    }

    // set the updated cookie
    setcookie( 'prayer_requests', $cookiedata, time() + 60 * 60 * 24 * 30, '/' ); // 30days
}
add_action( 'init', 'upr_update_cookie' );

function upr_get_witnesses_output() {
    return upr_get_generic_output( 'witnesses' );
}

function upr_get_prophecies_output() {
    return upr_get_generic_output( 'prophecies' );
}

function upr_get_prayers_output() {
    return upr_get_generic_output( 'prayers', 'all' );
}

function upr_get_prayers_next_output() {
    return upr_get_generic_output( 'prayers', 'next' );
}

function upr_get_prayers_lowest_pray_count_output() {
    return upr_get_generic_output( 'prayers', 'lowest' );
}

// main function for displaying prayer requests (used for our shortcodes)
/**
 * Show the prayers, witnesses, prophecies.
 * @param string $type prayers, witnesses, prophecies
 * @param string $kind next, all, lowest
 */
function upr_get_generic_output( $type, $kind = 'all' ) {
    global $wp_query, $wpdb;

	wp_reset_query();

	// find prayer requests
	$upr_no_prayer_per_page = get_option( 'upr_no_prayer_per_page' );

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	if ( $upr_no_prayer_per_page < 0 || $upr_no_prayer_per_page == '' ) {
		$upr_no_prayer_per_page = 10;
	}

	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );

    // output
    $html             = '';
    $upr_show_do_pray = true;
    
    if ( $kind == 'next' ) {
            if ( ! isset( $_COOKIE['prayer_requests'] ) ) {
                $prayer_requests = array();
            } else {
                $prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
            }

            $order                     = 'DESC';
            if ( $upr_prayer_fetch_req_from != 'all' ) {
                $prayers_args = array(
                    'exclude'        => array_keys( $prayer_requests ),
                    'numberposts'    => -1,
                    'post_type'      => 'prayers',
                    'posts_per_page' => 1,
                    'post_status'    => 'publish',
                    'order'          => $order,
                    'date_query'     => array(
                        'after' => date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ),
                    ),
                );
            } else {
                $prayers_args = array(
                    'exclude'        => array_keys( $prayer_requests ),
                    'numberposts'    => -1,
                    'post_type'      => 'prayers',
                    'posts_per_page' => 1,
                    'post_status'    => 'publish',
                    'order'          => $order,
                );
            }
            $prayers          = get_posts( $prayers_args );

            if ( count( $prayers ) == 0 ) {
                if ( $upr_prayer_fetch_req_from != 'all' ) {
                    $prayers_args = array(
                        'numberposts'    => -1,
                        'post_type'      => 'prayers',
                        'posts_per_page' => 99999999,
                        'post_status'    => 'publish',
                        'order'          => $order,
                        'date_query'     => array(
                            'after' => date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ),
                        ),
                    );
                } else {
                    $prayers_args = array(
                        'numberposts'    => -1,
                        'post_type'      => 'prayers',
                        'posts_per_page' => 99999999,
                        'post_status'    => 'publish',
                        'order'          => $order,
                    );
                }
                $allprayers = get_posts( $prayers_args );
                while ( count( $prayer_requests ) > 0 && count( $prayers ) == 0 ) {
                    $prayer_id = array_search( min( $prayer_requests ), $prayer_requests );
                    $prayer    = null;
                    // find the prayer with the id
                    foreach ( $allprayers as $p ) {
                        if ( $prayer_id == $p->ID ) {
                            $prayer = $p;
                        }
                    }
                    if ( ! is_null( $prayer ) ) {
                        $prayers = array( $prayer );
                    } else {
                        unset( $prayer_requests[ $prayer_id ] );
                    }
                }
            }

            if ( empty( $prayers ) ) {
                $html .= '<p>' . __( 'No Prayer Requests found.', 'prayers' ) . '</p>';
            }
    } elseif ( $kind == 'lowest' ) {
        // select the posts
        $sql = '
            SELECT p.*, pm.*
            FROM '.$wpdb->posts.' as p
            LEFT JOIN wp_postmeta as pm ON pm.post_id = p.ID and pm.meta_key = "prayers_count"
            WHERE post_type="prayers"
        ';
        if ($upr_prayer_fetch_req_from != 'all') {
            $sql .= ' AND post_date >= ' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) );
        }
        $sql .= ' AND post_status = "publish"
            ORDER BY CAST(pm.meta_value AS SIGNED) ASC, p.post_date ASC';
        $sql_total = $sql;
        
        $sql .= ' LIMIT '.(($paged-1) * intval($upr_no_prayer_per_page)).', '.intval($upr_no_prayer_per_page).'';

        $total_query = 'SELECT COUNT(1) FROM ('.$sql_total.') AS combined_table';
        $total_prayers = $wpdb->get_var( $total_query );

        $prayers          = $wpdb->get_results( $sql );
    } else {
        if ( $upr_prayer_fetch_req_from != 'all' ) {
            $prayers_args = array(
                'post_type'      => $type,
                'posts_per_page' => $upr_no_prayer_per_page,
                'post_status'    => 'publish',
                'paged'          => $paged,
                'order'          => 'DESC',
                'date_query'     => array(
                    'after' => date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ),
                ),
            );
        } else {
            $prayers_args = array(
                'post_type'      => $type,
                'posts_per_page' => $upr_no_prayer_per_page,
                'post_status'    => 'publish',
                'paged'          => $paged,
                'order'          => 'DESC',
            );
        }
        $prayers          = get_posts( $prayers_args );
    }

	// if we have prayers
	if ( $prayers ) {
		$current_url = get_permalink( get_the_ID() );
		$html       .= '<input type="hidden" id="current_url" value="' . $current_url . '">';
		$html       .= '<input type="hidden" id="admin-ajax" value="' . admin_url( 'admin-ajax.php' ) . '" />';
		$html       .= '<div class="wsl_prayer_engine">';
		$html       .= '<div class="wsl_prayer_enginelist">';
		$html       .= '<ul>';
		// foreach prayer request
		$registration = get_option( 'comment_registration' );
		global $current_user;
		get_currentuserinfo();
		if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
			$ip = '127.0.0.1';
		} else {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				// check ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				// to check ip is pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}
		if ( is_user_logged_in() ) {
			$uid     = $current_user->ID;
			$user_id = $current_user->display_name;
		} else {
			$uid = $current_user->ID;
		}

		$upr_hide_prayer_button               = get_option( 'upr_hide_prayer_button' );
		$upr_prayer_login_required_prayer     = get_option( 'upr_prayer_login_required_prayer' );
		$upr_hide_prayer_count                = get_option( 'upr_hide_prayer_count' );
		$upr_display_username                 = get_option( 'upr_display_username_on_prayer_listing' );
		$upr_allow_comments_prayer_request    = get_option( 'upr_allow_comments_prayer_request' );
		$upr_pray_prayed_button_ip            = get_option( 'upr_pray_prayed_button_ip' );
		$upr_time_interval_pray_prayed_button = get_option( 'upr_time_interval_pray_prayed_button' );

        if ( $type == 'prophecies' || $type == 'witnesses' ) {
            $upr_hide_prayer_button               = 1;// get_option('upr_hide_prayer_button');
            $upr_hide_prayer_count                = 1;// get_option('upr_hide_prayer_count');
            $upr_allow_comments_prayer_request    = 0;// get_option('upr_allow_comments_prayer_request');
        }

		if ( $upr_prayer_login_required_prayer != 1 ) {
			if ( is_user_logged_in() ) {
				$upr_show_do_pray = true;
			}
		} else {
			$upr_show_do_pray = true;
		}

		foreach ( $prayers as $prayer ) {
			$wp_prayer_id        = $prayer->ID;
			$wp_prayer_title     = get_the_title( $wp_prayer_id );
			$prayer_date         = get_the_date( 'l, j. F Y', $wp_prayer_id );
			$wp_prayer_thumbnail = get_the_post_thumbnail( $wp_prayer_id, 'thumbnail' );
			// $wp_prayer_content = apply_filters('the_content', $prayer->post_content);
			$wp_prayer_content = $prayer->post_content;

			if ( ! empty( $wp_prayer_content ) ) {
				$wp_prayer_content = strip_shortcodes( $wp_prayer_content );
			}
			$wp_prayer_permalink = get_permalink( $wp_prayer_id );
			$wp_prayers_name     = get_post_meta( $wp_prayer_id, 'prayers_name', true );
			$wp_prayers_email    = get_post_meta( $wp_prayer_id, 'prayers_email', true );
			$wp_prayers_website  = get_post_meta( $wp_prayer_id, 'prayers_website', true );
			$wp_prayers_count    = 0;
			if ( get_post_meta( $wp_prayer_id, 'prayers_count', true ) > 0 ) {
				$wp_prayers_count = get_post_meta( $wp_prayer_id, 'prayers_count', true );
			}

			/*
            // prayer performed
			$args             = array(
				'post_type'        => 'prayers_performed',
				'meta_query'       => array(
					array(
						'key'   => 'prayer_id',
						'value' => $wp_prayer_id,
					),
					array(
						'key'   => 'user_id',
						'value' => $user_id,
					),
				),
				'posts_per_page'   => -1,
				'suppress_filters' => false,
			);
			$prayer_performed = get_posts( $args );
			// end prayer performed
            */

			$html  = apply_filters( 'prayers_before_main_content', $html );
            if (file_exists(__DIR__.'/../view/prayer_request.php')) {
                include __DIR__.'/../view/prayer_request.php';
            } else {
                include __DIR__.'/../view/prayer_request.dist.php';
            }
		}
		$html .= '</ul>';
		$html .= '</div> <!-- wsl_prayer_engine -->';
		$html .= '</div> <!-- wsl_prayer_enginelist -->';
		$html .= '<div class="cf"></div>';
	}
    // todo get this working!
    if ( $kind == 'all' ) {
        $total_prayers   = get_posts(
            array(
                'post_type'      => $type,
                'posts_per_page' => -1,
                'post_status'    => 'publish',
            )
        );
        $max_num_pages   = ceil( sizeof( $total_prayers ) / $upr_no_prayer_per_page );
    } else {
        $max_num_pages   = ceil( $total_prayers / $upr_no_prayer_per_page );
    }

    $pagination_args = array(
        'base'         => $current_url . '%_%',
        'format'       => 'page/%#%',
        'total'        => $max_num_pages,
        'current'      => $paged,
        'show_all'     => false,
        'end_size'     => 1,
        'mid_size'     => 2,
        'prev_next'    => true,
        'prev_text'    => __( '&laquo;' ),
        'next_text'    => __( '&raquo;' ),
        'type'         => 'plain',
        'add_args'     => false,
        'add_fragment' => '',
    );
    $paginate_links  = paginate_links( $pagination_args );
    if ( $paginate_links ) {
        $html .= "<div align='center'><nav class='custom-pagination'>";
        $html .= "<span class='page-numbers page-num'>" . __( 'Page ', 'prayers' ) . $paged . __(
            ' of ',
            'prayers'
        ) . $max_num_pages . '</span> ';
        $html .= $paginate_links;
        $html .= '</nav></div>';
    }

	return $html;
}

function upr_list_all_output($type = null) {
    global $wp_query, $wpdb;

	wp_reset_query();
    
    if ( is_null( $type ) ) {
        $type = 'default';
    }
    
    // get type
    if ( isset( $_GET['orderBy'] ) ) {
        $type = $_GET['orderBy'];
        if ( ! in_array( $type, ['lowest', 'default'] ) ) {
            $type = 'default';
        }
    }

	// find prayer requests
	$upr_no_prayer_per_page = get_option( 'upr_no_prayer_per_page' );

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	if ( $upr_no_prayer_per_page < 0 || $upr_no_prayer_per_page == '' ) {
		$upr_no_prayer_per_page = 10;
	}

	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );

    // output
    $html             = '';
    
    if (get_option('upr_use_local_emojis') == 1) {
        $wp_get_upload_dir = wp_get_upload_dir()['baseurl'].'/emojis/';
        $typeIcons = [
            'witnesses' => '<span><img width="16" src="'.$wp_get_upload_dir.'2b50.svg"></a></span>',
            'prophecies' => '<span><img width="16" src="'.$wp_get_upload_dir.'1f5e8.svg"></a></span>',
            'prayers' => '<span><img width="16" src="'.$wp_get_upload_dir.'1f64f.svg"></a></span>',
            'comment' => '<span><img width="16" src="'.$wp_get_upload_dir.'1f4ac.svg"></a></span>',
        ];
    } else {
        $typeIcons = [
            'witnesses' => '<span>&#128488;</span>',
            'prophecies' => '<span>&#11088;</span>',
            'prayers' => '<span>&#128591;</span>',
        ];
    }
    /*
    ?>
    <style type="text/css">
.prayer-css-clearfix:before,
.prayer-css-clearfix:after {
    content: " "; 
    display: table;
}

.prayer-css-clearfix:after {
    clear: both;
}

.prayer-css-clearfix {
    *zoom: 1;
}
    </style>
    */
    
    ob_start();
    ?>
    <script>
// translation
var prayerjstranslation = {
    "commentsavedmessage": "<?=__( 'Comment saved. If your comment is not directly visible, then it must first be activated by an editor!', 'prayers' ) ?>",
    "acceptprivacy": "<?=__( 'You have to accept the privacy conditions!', 'prayers' ) ?>"
};
jQuery( document ).ready(function() {
    <?php /*
    jQuery(".show_item").click(function() {
        type = jQuery(this).attr("data-type");
        href = location.href;

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        
        const entries = urlParams.entries();
        var deleted = false;
        for(const entry of entries) {
            if (entry[0].indexOf("type") > -1) {
                // if type is in url, then delete it
                if (type == entry[1]) {
                    deleted = true;
                    href = href.replace(entry[0].replace("[", "%5B").replace("]", "%5D")+"="+entry[1], "");
                    href = href.replace(entry[0]+"="+entry[1], "");
                }
            }
        }
        // remove everything after anker #
        href = href.split('#')[0];
        // if type is not in url, then add it
        if (!deleted) {
            if (href.indexOf("?") === -1) {
                href += "?";
            } else {
                href += "&";
            }
            href += "type[]="+type;
        }
        // reload with new url
        window.location.href = href;
    });*/ ?>

    jQuery(".prayer-js-toggle-more").click(function() {
        var id = jQuery(this).attr("data-item-id");
        var more = document.getElementById("prayer_content_more_"+id);
        var less = document.getElementById("prayer-js-prayer-content-less-"+id);
        var buttonMore = document.getElementById("prayer-js-show-more-"+id);
        var buttonLess = document.getElementById("prayer-js-show-less-"+id);
        if (more.style.display === "none") {
            more.style.display = "block";
            less.style.display = "none";
            buttonMore.style.display = "none";
            buttonLess.style.display = "block";
        } else {
            more.style.display = "none";
            less.style.display = "block";
            buttonMore.style.display = "block";
            buttonLess.style.display = "none";
        }

<?php /*
        console.log(id);
        // todo geht nicht:
        jQuery("prayer_content_more_"+id).toggle();
        jQuery("prayer-js-prayer-content-less-"+id).toggle();
        jQuery("prayer-js-show-more-"+id).toggle();
        jQuery("prayer-js-show-less-"+id).toggle();


        var more = jQuery("prayer_content_more_"+id);
        var less = jQuery("prayer-js-prayer-content-less-"+id);
        var buttonMore = jQuery("prayer-js-show-more-"+id);
        var buttonLess = jQuery("prayer-js-show-less-"+id);
        if (more.css("display") === "none") {
            more.css("display", "block");
            less.css("display", "none");
            buttonMore.css("display",  "none");
            buttonLess.css("display", "block");
        } else {
            more.css("display", "none");
            less.css("display", "block");
            buttonMore.css("display", "block");
            buttonLess.css("display", "none");
        }

        */ ?>
    }); 
});
    </script>
    <?php
    $html .= ob_get_clean();
    
    $upr_show_do_pray = true;

    // get types
    $types = isset($_GET['type']) ? $_GET['type'] : ['prophecies', 'witnesses', 'prayers'];
    
    // todo if this text is set in WP text before shortcodes then the filters div are ABOVE this text but the text should be before the filter?!...
    ob_start();
    ?>
    <div style="text-align: center">
        <br>
        <?php _e( 'We believe that prayer moves the heavens and thus has an impact on our lives. Here we want to share requests, witnesses and prophecies to make us as a church one in prayer.', 'prayers' ); ?>
    </div>
    <div>
        <div>
            <?php
            /*
             https://www.iconfinder.com/search/?q=filter 
             */
            ?>
            <span style="text-decoration: underline">
                <img style="height:18px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAEgElEQVR4Xu2aWchWVRSGH01twqlBKkQciCCDIrUyh1RERRHFyKKLjKioLixFDCUVEssbcwBxAMERh1KMiGgeKM1SscjoQpAMUYkgTDRLTV7bn34c1/7OuP//8P9n3Xzwnb3Xetd79vSus9vQyq1NK8+fioBqBLRyBqJToAvwMnA3tLjp8R/wM7AU+LP23usJaAd8D9zXwgfFAWAAcE551hMwHPishSdfS28E8HlFQGQEaArsBe5t4aPgB6C/NQWUdy9gD3CrhwRNkT9KTtDNgIa4Zb8DDwKHrUWw9t/DwKfAdYaHr4GRwNmSktAe+BDQeha1f4BRwJf1D3wnwcnAFs9WuB6YUlICVgPPGdi0BT4FbIw+a3QUfh2Y40l0NvBmyUgQpgUeTMplnvWsEQF6Jsae9DCq/zVKymCTgLeBtgaYdwCNaI2CqyxODGkd0MI30Oh7xs01LZrNafcDXwE3GiC0qz0CnPYBjCNA/W5xO0Nvw8lx4AHgt2Zi4A6HrbsR/1e34p9ohC0JAeovbbAL6Gw409FyCHCqiUm4AfjCHWujof8CBgM/xmFKSoD8jAHeA3RgitoHwHjgfFzAgp5rru8AJhj+LgATHdbYcGkIkDMpxSUer4uAGbERi2mgWNM9roRxWdIwaQmQ3+XAS54ALwIrkwbP2O4ZYI2nr/5/No3fLARcA7wLjDMC/QuMBT5JAyJF26HAx0AHo89HDtMlmZvUshAg352Ab4B7jEAnAR2nDyYFkbDdXcBuoKvR/he3VV8udCT0mavq09NtQd2MYBIbEh0SH0XYTcC3wJ2GM4mzh4BDWQJlHQG1WIOccLrWCF6UcIoTOKPddpgl/1wjoBbwcWCzRzitA57OhOxKp1XA84YPHW0lyjbk8Z93BNRizwde8wCZBSzMCFJ93/D0Vcy5Gf1e7lYUASGEU5zA0cjToSeXFUWAQEg4qdCoBSlqEk7DgO8Som0kcPYB2g69AidhjEvNiiRA/m5zO0MPA8QxtzPECadGAueo86HfQqxoAgQqj3C63pWsVLePmsSWBI6KmoVZCAIETqdEnRZ1aoyaRMyjngy2A5r7UZPIkvB5v7DMnaNQBMj9K8BiD+B+wP7IM817zW/LpjUQYbk4CUmAgK0AXjAQqkSlEla9PQZsM9pKXElkBbHQBKh2IIEUNW1h0WRFylajrU6CqQROGqZCEyAsVjEyDQFBMQZ17t5ERUDOKRD0JQV1Xo2A/xmopkA1BfJtg0GnaVDn1RpQrQHVIljtAtU2WJ0DqoOQdRI8ArwFqOb/t9sufXI46FYd1LlLTOUs6+6OHusWh+r+a903faseEBRjUOeOgJ+AvjFFChGhD59PGO2CYgzq3CWjW5s7gY5pKjV1bYNiDOq8LonbgVfdNz6VvtNYUIxBnRtZ6lO6rrZMBZISERRjUOcNXnMaIoJiDOo8wTjXZ7CZDaaGLlhYFzASuE7WpLkJqKH0EaHP3/oMHszKQkA9Ebrt3cdde98ULHPnuGwEhM73Kv8VAU1OeckCViOgZC+kyeFcBM7E0kHZFkOZAAAAAElFTkSuQmCC" alt="filter icon">
                <?=_e( 'Filter', 'prayers' ) ?>:
            </span>
            <br>
            <a style="<?=(!in_array('witnesses', $types) ? 'color: grey' : '')?>" href="<?php echo get_permalink(); ?>?type[]=witnesses"><?php _e( 'Hearing', 'prayers' ) ?> 
                <?= $typeIcons['witnesses'] ?></a>
            <br>
            <a style="<?=(!in_array('prayers', $types) ? 'color: grey' : '')?>" href="<?php echo get_permalink(); ?>?type[]=prayers"><?php _e( 'Request', 'prayers' ) ?> 
                <?= $typeIcons['prayers'] ?>
            <br>
            <a style="<?=(!in_array('prophecies', $types) ? 'color: grey' : '')?>" href="<?php echo get_permalink(); ?>?type[]=prophecies"><?php _e( 'Impression', 'prayers' ) ?> 
                <?= $typeIcons['prophecies'] ?></a>
            <?php
            if ( isset( $_GET['type'] ) && ! empty( $_GET['type'] ) ) {
                ?>
                <br>
                <a href="<?php echo get_permalink(); ?>"><?php _e( 'Show all', 'prayers' ) ?></a>
                <?php
            }
            ?>
            <?php
            /*<span style="cursor: pointer; <?=(!in_array('witnesses', $types) ? 'color: grey' : '')?>"
                data-type="witnesses" class="show_item show_witness"><?php _e( 'Witness', 'prayers' ) ?> 
                <?= $typeIcons['witnesses'] ?>
            </span>
            <br>
            <span style="cursor: pointer; <?=(!in_array('prayers', $types) ? 'color: grey' : '')?>"
                data-type="prayers" class="show_item show_prayer_request"><?php _e( 'Request', 'prayers' ) ?> 
                <?= $typeIcons['prayers'] ?>
            </span>
            <br>
            <span style="cursor: pointer; <?=(!in_array('prophecies', $types) ? 'color: grey' : '')?>"
                data-type="prophecies" class="show_item show_prophecy"><?php _e( 'Prophecy', 'prayers' ) ?> 
                <?= $typeIcons['prophecies'] ?>
            </span>
            */ ?>
            <br>
            <br>
            <?php
            if ( get_option( 'upr_disable_hide_all_prayed' ) != 1 ) {
                ?>
                <span>
                    <?php
                    if ( isset( $_GET['hidePrayed'] ) ) {
                        ?>
                        <a href="<?php echo get_permalink(get_the_ID()) ?>"><?php _e( 'Show all', 'prayers' ) ?></a>
                        <?php
                    } else {
                        ?>
                        <a href="<?php echo get_permalink(get_the_ID()) ?>?hidePrayed"><?php _e( 'Hide all prayed', 'prayers' ) ?></a>
                        <?php
                    }
                    ?>
                </span>
            <?php
            }
            ?>
        </div>
        <div>
            <?php _e( 'Order By', 'prayers' ) ?>: 
            <a style="<?php echo ( $type == 'default' ? 'font-weight: bold' : '' ) ?>" href="<?php echo get_permalink(get_the_ID()) ?>?orderBy=default"><?php _e( 'Newest First', 'prayers' ) ?></a>
            |
            <a style="<?php echo ( $type == 'lowest' ? 'font-weight: bold' : '' ) ?>" href="<?php echo get_permalink(get_the_ID()) ?>?orderBy=lowest"><?php _e( 'Lowest Pray Count First', 'prayers' ) ?></a>
        </div>
    </div>
    <?php
    $html .= ob_get_clean();
    
    if ($type == 'lowest') {
        $query = '
            SELECT p.*, pm.*
            FROM ' . $wpdb->posts . ' p
            LEFT JOIN wp_postmeta as pm ON pm.post_id = p.ID and pm.meta_key = "prayers_count"
            WHERE post_status = "publish"
            AND (';

        $orderBy = ' ORDER BY CAST(pm.meta_value AS SIGNED) ASC, p.post_date DESC';
    } else {
        $query = '
            SELECT *
            FROM ' . $wpdb->posts . ' p
            WHERE post_status = "publish"
            AND (';

        $orderBy = ' ORDER BY post_date DESC';
    }

    $typeQuery = '';
    $i = 0;
    if ( empty( $types ) ) {
        $types = ['prophecies', 'witnesses', 'prayers'];
    }
    foreach ( $types as $type ) {
        if ( in_array( $type, ['prophecies', 'witnesses', 'prayers'] ) ) {
            if ( $i > 0 ) {
                $typeQuery .= ' OR ';
            }
            $typeQuery .= ' post_type = "' . $type . '"';
            $i++;
        }
    }

    $query .= $typeQuery . ')';
    $upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
    $queryPostDate = '';
    $queryBeforeLimit = '';
    if ( $upr_prayer_fetch_req_from != 'all' ) {
        // todo this affects all types!! so not only prayers but also witnesses...
        $queryPostDate = ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
        $query .= $queryPostDate;
    }

    // hide the prayed items
    $queryHidePrayed = '';
    if ( isset( $_GET['hidePrayed'] ) ) {
        if ( ! isset( $_COOKIE['prayer_requests'] ) ) {
            $pr_ids = array();
        } else {
            $cookie_prayer_requests = json_decode( stripslashes( $_COOKIE['prayer_requests'] ), true );
            $pr_ids = array_keys( $cookie_prayer_requests );
            // security, only ints
            foreach ( $pr_ids as &$id ) {
                $id = intval($id);
            }
        }
        if ( ! empty( $pr_ids ) ) {
            $queryHidePrayed = ' AND ID NOT IN (' . implode( ', ', $pr_ids ) .')';
        }
    }
    $query .= $queryHidePrayed;

    $queryBeforeLimit .= $query . $orderBy;

    $total_query = 'SELECT COUNT(1) FROM ('.$queryBeforeLimit.') AS combined_table';
    $total_prayers = $wpdb->get_var( $total_query );

    $query = $queryBeforeLimit
        . ' LIMIT ' . intval(($paged-1) * $upr_no_prayer_per_page) . ', ' . $upr_no_prayer_per_page;
        
    $queryResult = $wpdb->get_results( $query );

    // if empty, then go to startpage
    if ( empty( $queryResult ) ) {
        $query = $queryBeforeLimit
        . ' LIMIT ' . intval(0 * $upr_no_prayer_per_page) . ', ' . $upr_no_prayer_per_page;
    
        $queryResult = $wpdb->get_results( $query );
        $paged = 1;
    }

    // show message if there are no items
    if ( empty( $queryResult ) ) {
        echo '<div class="wsl_prayer_engine" style="color: #ff0000;">'
            . __( 'The query returned no elements, maybe there is nothing or your filters are too strict.', 'prayers' )
            . '</div>';
    }

	// if we have prayers
	if ( $queryResult ) {
		$current_url = get_permalink( get_the_ID() );
		$html       .= '<input type="hidden" id="prayer-js-current-url" value="' . $current_url . '">';
		$html       .= '<input type="hidden" id="admin-ajax" value="' . admin_url( 'admin-ajax.php' ) . '" />';
		$html       .= '<div class="wsl_prayer_engine">';
		$html       .= '<div class="wsl_prayer_enginelist">';
		$html       .= '<ul>';
		// foreach prayer request
		$registration = get_option( 'comment_registration' );
		global $current_user;
		get_currentuserinfo();
		if ( get_option( 'upr_disable_ip_logging' ) == 1 ) {
			$ip = '127.0.0.1';
		} else {
			if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
				// check ip from share internet
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
				// to check ip is pass from proxy
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}
		if ( is_user_logged_in() ) {
			$uid     = $current_user->ID;
			$user_id = $current_user->display_name;
		} else {
			$uid = $current_user->ID;
		}

		$upr_hide_prayer_button               = get_option( 'upr_hide_prayer_button' );
		$upr_prayer_login_required_prayer     = get_option( 'upr_prayer_login_required_prayer' );
		$upr_hide_prayer_count                = get_option( 'upr_hide_prayer_count' );
		$upr_display_username                 = get_option( 'upr_display_username_on_prayer_listing' );
		$upr_allow_comments_prayer_request    = get_option( 'upr_allow_comments_prayer_request' );
		$upr_pray_prayed_button_ip            = get_option( 'upr_pray_prayed_button_ip' );
		$upr_time_interval_pray_prayed_button = get_option( 'upr_time_interval_pray_prayed_button' );

        if ( $type == 'prophecies' || $type == 'witnesses' ) {
            $upr_hide_prayer_button               = 1;// get_option('upr_hide_prayer_button');
            $upr_hide_prayer_count                = 1;// get_option('upr_hide_prayer_count');
            $upr_allow_comments_prayer_request    = 0;// get_option('upr_allow_comments_prayer_request');
        }

		if ( $upr_prayer_login_required_prayer != 1 ) {
			if ( is_user_logged_in() ) {
				$upr_show_do_pray = true;
			}
		} else {
			$upr_show_do_pray = true;
		}

		foreach ( $queryResult as $prayer ) {
			$wp_prayer_id        = $prayer->ID;
			$wp_prayer_title     = get_the_title( $wp_prayer_id );
			$prayer_date         = get_the_date( 'l, j. F Y', $wp_prayer_id );
			$wp_prayer_thumbnail = get_the_post_thumbnail( $wp_prayer_id, 'thumbnail' );
			// $wp_prayer_content = apply_filters('the_content', $prayer->post_content);
			$wp_prayer_content = $prayer->post_content;

			if ( ! empty( $wp_prayer_content ) ) {
				$wp_prayer_content = strip_shortcodes( $wp_prayer_content );
			}
			$wp_prayer_permalink = get_permalink( $wp_prayer_id );
			$wp_prayers_name     = get_post_meta( $wp_prayer_id, 'prayers_name', true );
			$wp_prayers_email    = get_post_meta( $wp_prayer_id, 'prayers_email', true );
			$wp_prayers_website  = get_post_meta( $wp_prayer_id, 'prayers_website', true );
			$wp_prayers_count    = 0;
			if ( get_post_meta( $wp_prayer_id, 'prayers_count', true ) > 0 ) {
				$wp_prayers_count = get_post_meta( $wp_prayer_id, 'prayers_count', true );
			}

            /*
			// prayer performed
			$args             = array(
				'post_type'        => 'prayers_performed',
				'meta_query'       => array(
					array(
						'key'   => 'prayer_id',
						'value' => $wp_prayer_id,
					),
					array(
						'key'   => 'user_id',
						'value' => $user_id,
					),
				),
				'posts_per_page'   => -1,
				'suppress_filters' => false,
			);
			$prayer_performed = get_posts( $args );
			// end prayer performed
            */

			$html  = apply_filters( 'prayers_before_main_content', $html );
            if (file_exists(__DIR__.'/../view/show_item.php')) {
                include __DIR__.'/../view/show_item.php';
            } else {
                include __DIR__.'/../view/show_item.dist.php';
            }
		}
		$html .= '</ul>';
		$html .= '</div> <!-- wsl_prayer_engine -->';
		$html .= '</div> <!-- wsl_prayer_enginelist -->';
		//$html .= '<div class="cf"></div>';
	}

// todo for lowest!
    /*
    // pagination
    $query = '
        SELECT count(*)
        FROM ' . $wpdb->posts . ' p
        WHERE post_status = "publish"
        AND (';

    $query .= $typeQuery . ')';
    $query .= $queryPostDate;
    $query .= $queryHidePrayed;

    $count_total = $wpdb->get_var( $query );
    */

    $max_num_pages   = ceil( $total_prayers / $upr_no_prayer_per_page );
    $pagination_args = array(
        'base'         => $current_url . '%_%',
        'format'       => 'page/%#%',
        'total'        => $max_num_pages,
        'current'      => $paged,
        'show_all'     => false,
        'end_size'     => 1,
        'mid_size'     => 2,
        'prev_next'    => true,
        'prev_text'    => __( '&laquo;' ),
        'next_text'    => __( '&raquo;' ),
        'type'         => 'plain',
        'add_args'     => false,
            'add_fragment' => '',
    );
    $paginate_links  = paginate_links( $pagination_args );
    if ( $paginate_links ) {
        $html .= "<div align='center'><nav class='custom-pagination'>";
        $html .= "<span class='page-numbers page-num'>" . __( 'Page ', 'prayers' ) . $paged . __(
            ' of ',
            'prayers'
        ) . $max_num_pages . '</span> ';
        $html .= $paginate_links;
        $html .= '</nav></div>';
    }
    //$html .= '<br><br><div>Filter Icon erstellt von <a href="https://www.flaticon.com/de/autoren/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/de/" title="Flaticon">www.flaticon.com</a></div>';

	return $html;
}
?>
