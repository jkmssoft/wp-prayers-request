<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Nope, not accessing this' );} // Exit if accessed directly ?>
<div class="wrap">
	<h1><?php _e( 'Email Templates Settings', 'prayers' ); ?></h1>
	<?php
	if ( isset( $_POST['emailsettings'] ) ) {
		$prayer_req_admin_email      = isset( $_POST['prayer_req_admin_email'] ) ? sanitize_email( $_POST['prayer_req_admin_email'] ) : '';
		$prayer_admin_email_cc       = isset( $_POST['prayer_admin_email_cc'] ) ? sanitize_email( $_POST['prayer_admin_email_cc'] ) : '';
		$prayer_email_from           = isset( $_POST['prayer_email_from'] ) ? sanitize_text_field( $_POST['prayer_email_from'] ) : '';
		$prayer_email_user           = isset( $_POST['prayer_email_user'] ) ? sanitize_email( $_POST['prayer_email_user'] ) : '';
		$prayer_email_req_subject    = isset( $_POST['prayer_email_req_subject'] ) ? sanitize_text_field( $_POST['prayer_email_req_subject'] ) : '';
		$prayer_email_req_messages   = isset( $_POST['prayer_email_req_messages'] ) ? sanitize_text_field( $_POST['prayer_email_req_messages'] ) : '';
		$prayer_email_admin_subject  = isset( $_POST['prayer_email_admin_subject'] ) ? sanitize_text_field( $_POST['prayer_email_admin_subject'] ) : '';
		$prayer_email_admin_messages = isset( $_POST['prayer_email_admin_messages'] ) ? sanitize_text_field( $_POST['prayer_email_admin_messages'] ) : '';

		update_option( 'prayer_req_admin_email', $prayer_req_admin_email );
		update_option( 'prayer_admin_email_cc', $prayer_admin_email_cc );
		update_option( 'prayer_email_from', $prayer_email_from );
		update_option( 'prayer_email_user', $prayer_email_user );
		update_option( 'prayer_email_req_subject', $prayer_email_req_subject );
		update_option( 'prayer_email_req_messages', $prayer_email_req_messages );
		update_option( 'prayer_email_admin_subject', $prayer_email_admin_subject );
		update_option( 'prayer_email_admin_messages', $prayer_email_admin_messages );
		echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"><p><strong>' . __( 'Settings saved', 'prayers' ) . '</strong></p></div>';
	}
		$prayer_req_admin_email      = get_option( 'prayer_req_admin_email' );
		$prayer_admin_email_cc       = get_option( 'prayer_admin_email_cc' );
		$prayer_email_from           = get_option( 'prayer_email_from' );
		$prayer_email_user           = get_option( 'prayer_email_user' );
		$prayer_email_req_subject    = get_option( 'prayer_email_req_subject' );
		$prayer_email_req_messages   = get_option( 'prayer_email_req_messages' );
		$prayer_email_admin_subject  = get_option( 'prayer_email_admin_subject' );
		$prayer_email_admin_messages = get_option( 'prayer_email_admin_messages' );
	?>
	<form method="post" action="" novalidate>
		<table class="form-table">
			<tbody>
				<tr>
					<td colspan="2"><h2><?php _e( 'Sender Email Settings', 'prayers' ); ?></h2></td>
				</tr>
				<tr>
					<th scope="row"><label for="blogname"><?php _e( 'Admin Email', 'prayers' ); ?></label></th>
					<td><input name="prayer_req_admin_email" id="prayer_req_admin_email" value="<?php echo $prayer_req_admin_email; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'Add CC Admin Email', 'prayers' ); ?></label></th>
					<td><input name="prayer_admin_email_cc" id="prayer_admin_email_cc" value="<?php echo $prayer_admin_email_cc; ?>" class="regular-text" type="text">
					<p class="description"><?php _e( 'Add CC admin email addresses, separate them with comma', 'prayers' ); ?></p></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'From Name', 'prayers' ); ?></label></th>
					<td><input name="prayer_email_from" id="prayer_email_from" value="<?php echo $prayer_email_from; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'From Email Address', 'prayers' ); ?></label></th>
					<td><input name="prayer_email_user" id="prayer_email_user" value="<?php echo $prayer_email_user; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<td colspan="2"><h2><?php _e( 'User notification email Message', 'prayers' ); ?></h2></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'Prayer Request Subject', 'prayers' ); ?> </label></th>
					<td><input name="prayer_email_req_subject" id="prayer_email_req_subject" value="<?php echo $prayer_email_req_subject; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'Prayer Request Message', 'prayers' ); ?></label></th>
					<td><textarea rows="5" cols="100" name="prayer_email_req_messages"><?php echo $prayer_email_req_messages; ?></textarea></td>
				</tr>
				<tr>
					<td colspan="2"><h2><?php _e( 'Admin notification email Message', 'prayers' ); ?></h2></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'Admin Email Subject', 'prayers' ); ?> </label></th>
					<td><input name="prayer_email_admin_subject" id="prayer_email_admin_subject" value="<?php echo $prayer_email_admin_subject; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<th scope="row"><label><?php _e( 'Admin Email Message', 'prayers' ); ?></label></th>
					<td><textarea rows="5" cols="100" name="prayer_email_admin_messages"><?php echo $prayer_email_admin_messages; ?></textarea></td>
				</tr>
			</tbody>
		</table>
		<p class="submit"><input name="emailsettings" id="submit" class="button button-primary" value="<?php _e( 'Update', 'prayers' ); ?>" type="submit"></p
	></form>
</div>
