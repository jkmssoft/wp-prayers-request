<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Nope, not accessing this' );} // Exit if accessed directly ?>
<div class="wrap">
	<h1><?php _e( 'Statistic', 'prayers' ); ?></h1>
    
    <?php
    // count prayers performed per day
    $query                     = '
        SELECT date(post_date) as date, count(post_date) as count
        FROM ' . $GLOBALS['wpdb']->posts . ' p
        WHERE post_type = "prayers_performed"
        AND post_status = "publish"
    ';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$query .= ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
	}
    $query .= ' GROUP BY date(post_date)';
    $query .= ' ORDER BY date(post_date) DESC;';
	$queryResult = $GLOBALS['wpdb']->get_results( $query );
    
    // new prayer requests per day
    $queryPR                     = '
        SELECT date(post_date) as date, count(post_date) as count
        FROM ' . $GLOBALS['wpdb']->posts . ' p
        WHERE post_type = "prayers"
        AND post_status = "publish"
    ';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$queryPR .= ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
	}
    $queryPR .= ' GROUP BY date(post_date)';
    $queryPR .= ' ORDER BY date(post_date) DESC;';
	$prayersResult = $GLOBALS['wpdb']->get_results( $queryPR );
	$prayers = array();
    foreach ($prayersResult as $p ) {
        $prayers[$p->date] = $p->count;
    }
    unset($prayersResult);
    
    // witnesses
    $queryPR                     = '
        SELECT date(post_date) as date, count(post_date) as count
        FROM ' . $GLOBALS['wpdb']->posts . ' p
        WHERE post_type = "witnesses"
        AND post_status = "publish"
    ';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$queryPR .= ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
	}
    $queryPR .= ' GROUP BY date(post_date)';
    $queryPR .= ' ORDER BY date(post_date) DESC;';
	$result = $GLOBALS['wpdb']->get_results( $queryPR );
	$witnesses = array();
    foreach ($result as $p ) {
        $witnesses[$p->date] = $p->count;
    }
    unset($result);

    // prophecies
    $queryPR                     = '
        SELECT date(post_date) as date, count(post_date) as count
        FROM ' . $GLOBALS['wpdb']->posts . ' p
        WHERE post_type = "prophecies"
        AND post_status = "publish"
    ';
	$upr_prayer_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' );
	if ( $upr_prayer_fetch_req_from != 'all' ) {
		$queryPR .= ' AND post_date >= "' . date( 'Y-m-d', strtotime( '-' . $upr_prayer_fetch_req_from . ' days' ) ) . '"';
	}
    $queryPR .= ' GROUP BY date(post_date)';
    $queryPR .= ' ORDER BY date(post_date) DESC;';
	$result = $GLOBALS['wpdb']->get_results( $queryPR );
	$prophecies = array();
    foreach ($result as $p ) {
        $prophecies[$p->date] = $p->count;
    }
    unset($result);

    echo '<table border="1">
        <tr>
            <th>' . __( 'Date', 'prayers' ) .' </th>
            <th>' . __( 'Prayers Performed', 'prayers' ) .' </th>
            <th>' . __( 'New Prayer Requests', 'prayers' ) .' </th>
            <th>' . __( 'New Prophecies', 'prayers' ) .' </th>
            <th>' . __( 'New Witnesses', 'prayers' ) .' </th>
        </tr>';
    foreach ( $queryResult as $result ) {
        echo '<tr>
            <td>' . $result->date . '</td>
            <td>' . $result->count . '</td>
            <td>' . (isset($prayers[$result->date]) ? $prayers[$result->date] : 0) . '</td>
            <td>' . (isset($prophecies[$result->date]) ? $prophecies[$result->date] : 0) . '</td>
            <td>' . (isset($witnesses[$result->date]) ? $witnesses[$result->date] : 0) . '</td>
        </tr>';
    }
    echo '</table>';
    ?>
</div>
