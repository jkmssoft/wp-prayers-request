<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Nope, not accessing this' );} // Exit if accessed directly ?>
<div class="wrap">
	<h1><?php _e( 'Setup Settings', 'prayers' ); ?></h1>
	<?php
	if ( isset( $_POST['prayerssettings'] ) ) {
		update_option( 'upr_prayer_list_title', $_POST['upr_prayer_list_title'] );
		update_option( 'upr_no_prayer_per_page', $_POST['upr_no_prayer_per_page'] );
		update_option( 'upr_prayer_page_slug', $_POST['upr_prayer_page_slug'] );
		update_option( 'upr_accept_privacy', stripslashes( trim( $_POST['upr_accept_privacy'] ) ) ); // html!
		update_option( 'upr_login_not_required_request', 0 );
		update_option( 'upr_prayer_login_required_prayer', 0 );
		update_option( 'upr_prayer_send_email', 0 );
		update_option( 'upr_prayer_send_admin_email', 0 );
		update_option( 'upr_prayer_default_status_pending', 0 );
		update_option( 'upr_hide_prayer_button', 0 );
		update_option( 'upr_hide_prayer_count', 0 );
		update_option( 'upr_display_username_on_prayer_listing', 0 );
		update_option( 'upr_prayer_hide_captcha', 1 );
		update_option( 'upr_prayer_spam_prevention', 0 );
		update_option( 'upr_prayer_spam_DDoS_prevention', 0 );
		update_option( 'upr_prayer_show_country', 0 );
		update_option( 'upr_allow_comments_prayer_request', 0 );
		update_option( 'upr_pray_prayed_button_ip', 0 );
		update_option( 'upr_show_prayer_category', 0 );
		update_option( 'upr_time_interval_pray_prayed_button', $_POST['upr_time_interval_pray_prayed_button'] );
		update_option( 'upr_prayer_fetch_req_from', $_POST['upr_prayer_fetch_req_from'] );
		update_option( 'upr_disable_ip_logging', 0 );
		update_option( 'upr_use_local_emojis', 0 );
		update_option( 'upr_disable_hide_all_prayed', 0 );
		if ( isset( $_POST['upr_show_prayer_category'] ) ) {
			update_option( 'upr_show_prayer_category', 1 );
		}
		if ( isset( $_POST['upr_login_not_required_request'] ) ) {
			update_option( 'upr_login_not_required_request', 1 );
		}
		if ( isset( $_POST['upr_prayer_login_required_prayer'] ) ) {
			update_option( 'upr_prayer_login_required_prayer', 1 );
		}
		if ( isset( $_POST['upr_prayer_default_status_pending'] ) ) {
			update_option( 'upr_prayer_default_status_pending', 1 );
		}
		if ( isset( $_POST['upr_hide_prayer_button'] ) ) {
			update_option( 'upr_hide_prayer_button', 1 );
		}
		if ( isset( $_POST['upr_hide_prayer_count'] ) ) {
			update_option( 'upr_hide_prayer_count', 1 );
		}
		if ( isset( $_POST['upr_prayer_send_email'] ) ) {
			update_option( 'upr_prayer_send_email', 1 );
		}
		if ( isset( $_POST['upr_prayer_send_admin_email'] ) ) {
			update_option( 'upr_prayer_send_admin_email', 1 );
		}
		if ( isset( $_POST['upr_display_username_on_prayer_listing'] ) ) {
			update_option( 'upr_display_username_on_prayer_listing', 1 );
		}
		if ( isset( $_POST['upr_prayer_hide_captcha'] ) ) {
			update_option( 'upr_prayer_hide_captcha', 0 );
		}
		if ( isset( $_POST['upr_prayer_spam_prevention'] ) ) {
			update_option( 'upr_prayer_spam_prevention', 1 );
		}
		if ( isset( $_POST['upr_prayer_spam_DDoS_prevention'] ) ) {
			update_option( 'upr_prayer_spam_DDoS_prevention', 1 );
		}
		if ( isset( $_POST['upr_allow_comments_prayer_request'] ) ) {
			update_option( 'upr_allow_comments_prayer_request', 1 );
		}
		if ( isset( $_POST['upr_pray_prayed_button_ip'] ) ) {
			update_option( 'upr_pray_prayed_button_ip', 1 );
		}
		if ( isset( $_POST['upr_prayer_show_country'] ) ) {
			update_option( 'upr_prayer_show_country', 1 );
		}
		if ( isset( $_POST['upr_disable_ip_logging'] ) ) {
			update_option( 'upr_disable_ip_logging', 1 );
		}
        if ( isset( $_POST['upr_use_local_emojis'] ) ) {
			update_option( 'upr_use_local_emojis', 1 );
		}
        if ( isset( $_POST['upr_disable_hide_all_prayed'] ) ) {
			update_option( 'upr_disable_hide_all_prayed', 1 );
		}
		if ( isset( $_POST['secret_gc'] ) ) {
			$key = $_POST['secret_gc'];
			update_option( 'upr_prayer_secret', $key );
		}
		if ( isset( $_POST['sitekey_gc'] ) ) {
			$key1 = $_POST['sitekey_gc'];
			update_option( 'upr_prayer_sitekey', $key1 );
		}
		echo '<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible">
			<p><strong>' . __( 'Updated' ) . '</strong></p></div>';
	}
	?>
	<form method="post" action="" novalidate>
		<table class="form-table">
			<tbody>
				<?php
				$upr_prayer_list_title                  = get_option( 'upr_prayer_list_title' );
				$upr_no_prayer_per_page                 = get_option( 'upr_no_prayer_per_page' );
				$upr_login_not_required_request         = get_option( 'upr_login_not_required_request' );
				$upr_prayer_login_required_prayer       = get_option( 'upr_prayer_login_required_prayer' );
				$upr_prayer_send_email                  = get_option( 'upr_prayer_send_email' );
				$upr_prayer_send_admin_email            = get_option( 'upr_prayer_send_admin_email' );
				$upr_prayer_default_status_pending      = get_option( 'upr_prayer_default_status_pending' );
				$upr_hide_prayer_button                 = get_option( 'upr_hide_prayer_button' );
				$upr_hide_prayer_count                  = get_option( 'upr_hide_prayer_count' );
				$upr_display_username_on_prayer_listing = get_option( 'upr_display_username_on_prayer_listing' );
				$upr_prayer_hide_captcha                = get_option( 'upr_prayer_hide_captcha' );
				$upr_prayer_spam_prevention             = get_option( 'upr_prayer_spam_prevention' );
				$upr_prayer_spam_DDoS_prevention        = get_option( 'upr_prayer_spam_DDoS_prevention' );
				$upr_prayer_show_country                = get_option( 'upr_prayer_show_country' );
				$upr_prayer_secret                      = get_option( 'upr_prayer_secret' );
				$upr_prayer_sitekey                     = get_option( 'upr_prayer_sitekey' );
				$upr_prayer_page_slug                   = get_option( 'upr_prayer_page_slug' );
				?>
				<tr>
					<td><label><?php _e( 'Prayer List Title', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_list_title" id="prayer_list_title"
							   value="<?php echo $upr_prayer_list_title; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<td><label><?php _e( 'Prayer Page Slug', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_page_slug" id="prayer_list_title"
							   value="<?php echo $upr_prayer_page_slug; ?>" class="regular-text" type="text"> <?_e('If this is set cookie cleanup will only be executed on this page!', 'prayers')?></td>
				</tr>
				<tr>
					<td><label><?php _e( 'No. of prayers per page', 'prayers' ); ?></label></td>
					<td><input name="upr_no_prayer_per_page" id="no_prayer_per_page" value="<?php echo $upr_no_prayer_per_page; ?>" class="regular-text" type="text"></td>
				</tr>
				<tr>
					<td scope="row"><label><?php _e( 'Login not required for prayer request', 'prayers' ); ?></label></td>
					<td><input name="upr_login_not_required_request" id="login_not_required_request" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_login_not_required_request == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Login is not required to submit the prayer request.', 'prayers' ); ?></td>
				</tr>
				<tr>
					<td scope="row"><label><?php _e( 'Login not required for prayer', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_login_required_prayer" id="prayer_login_required_prayer" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_login_required_prayer == 1 ) {
						echo 'checked';}
					?>
					><?php _e( 'Login is not required to do pray', 'prayers' ); ?></td>
				</tr>
				<?php $upr_prayer_send_email = get_option( 'upr_prayer_send_email' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Send Notification To User', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_send_email" id="prayer_send_email" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_send_email == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'User is to be notified via email after submitting the prayer request.', 'prayers' ); ?></td>
				</tr>
				<?php $upr_send_admin_email = get_option( 'upr_prayer_send_admin_email' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Send Notification To Admin', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_send_admin_email" id="prayer_send_admin_email" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_send_admin_email == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Admin is to be notified via email whenever new prayer request is submitted.', 'prayers' ); ?></td>
				</tr>
				<?php $upr_prayer_default_status_pending = get_option( 'upr_prayer_default_status_pending' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Default requests to status pending', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_default_status_pending" id="prayer_default_status_pending" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_default_status_pending == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Default prayer requests to status pending.', 'prayers' ); ?></td>
				</tr>
				<?php $upr_hide_prayer_button = get_option( 'upr_hide_prayer_button' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Hide Prayer button', 'prayers' ); ?></label></td>
					<td><input name="upr_hide_prayer_button" id="hide_prayer_button" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_hide_prayer_button == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Hide Prayer button.', 'prayers' ); ?></td>
				</tr>
				<?php $upr_hide_prayer_count = get_option( 'upr_hide_prayer_count' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Hide Prayer count', 'prayers' ); ?></label></td>
					<td><input name="upr_hide_prayer_count" id="hide_prayer_count" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_hide_prayer_count == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Hide Prayer count', 'prayers' ); ?></td>
				</tr>
				<?php $upr_display_username_on_prayer_listing = get_option( 'upr_display_username_on_prayer_listing' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Display user name on prayer listing', 'prayers' ); ?></label></td>
					<td><input name="upr_display_username_on_prayer_listing" id="display_username_on_prayer_listing" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_display_username_on_prayer_listing == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Display user name on prayer listing', 'prayers' ); ?></td>
				</tr>
				<?php $upr_prayer_hide_captcha = get_option( 'upr_prayer_hide_captcha' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Disable captcha', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_hide_captcha" id="prayer_hide_captcha" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_hide_captcha == 0 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Disable captcha', 'prayers' ); ?></td>
				</tr>
				<tr>
					<td><label>Google reCaptcha v2 Site Key</label></td>
					<td><input class="regular-text" value="<?php echo $upr_prayer_sitekey; ?>" type="text" name="sitekey_gc" placeholder="6LeG_2QUAAAAAIw5Qj9eyTlt_sATdOmTHesbxdea"></td>
				</tr>
				<tr>
					<td><label>Google reCaptcha v2 Secret Key</label></td>
					<td><input name="secret_gc" class="regular-text" value="<?php echo $upr_prayer_secret; ?>"
							   type="text" placeholder="6LeG_2QUAAAAAF-N1s4TN_s_jDGIM9gfRuD0Hqpo"></td>
				</tr>
				<?php $upr_prayer_spam_prevention = get_option( 'upr_prayer_spam_prevention' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Spam Prevention', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_spam_prevention" id="prayer_spam_prevention" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_spam_prevention == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Enable spam prevention', 'prayers' ); ?></td>
				</tr>
				<?php $upr_prayer_spam_DDoS_prevention = get_option( 'upr_prayer_spam_DDoS_prevention' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Spam/DDoS prevention & detection', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_spam_DDoS_prevention" id="prayer_spam_DDoS_prevention" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_spam_DDoS_prevention == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Enable Spam/DDoS prevention & detection system', 'prayers' ); ?></td>
				</tr>
				<?php $upr_prayer_show_country = get_option( 'upr_prayer_show_country' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Show country', 'prayers' ); ?></label></td>
					<td><input name="upr_prayer_show_country" id="prayer_show_country" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_prayer_show_country == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Display country on form', 'prayers' ); ?></td>
				</tr>
				<?php $upr_show_prayer_category = get_option( 'upr_show_prayer_category' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Show prayer category', 'prayers' ); ?></label></td>
					<td><input name="upr_show_prayer_category" id="show_prayer_category" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_show_prayer_category == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Display prayer category on form', 'prayers' ); ?></td>
				</tr>      
				<?php $upr_allow_comments_prayer_request = get_option( 'upr_allow_comments_prayer_request' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Allow Comments', 'prayers' ); ?></label></td>
					<td><input name="upr_allow_comments_prayer_request" id="allow_comments_prayer_request" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_allow_comments_prayer_request == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'Allow comments on prayer comments', 'prayers' ); ?></td>
				</tr>              
				<?php $upr_pray_prayed_button_ip = get_option( 'upr_pray_prayed_button_ip' ); ?>
				<!--<tr>
					<td scope="row"><label><?php _e( 'Pray/Prayed button by IP address', 'prayers' ); ?></label></td>
					<td><input name="pray_prayed_button_ip" id="pray_prayed_button_ip" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_pray_prayed_button_ip == 1 ) {
						echo 'checked';}
					?>
					> <?php _e( 'On/Off', 'prayers' ); ?></td>
				</tr>-->
				<?php $upr_time_interval_pray_prayed_button = get_option( 'upr_time_interval_pray_prayed_button' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Time interval between Pray/Prayed button', 'prayers' ); ?></label></td>
					<td><input name="upr_time_interval_pray_prayed_button" id="upr_time_interval_pray_prayed_button" value="<?php echo $upr_time_interval_pray_prayed_button; ?>"  type="text" size="10"> <?php _e( '(in seconds)', 'prayers' ); ?></td>
				</tr>          
				<?php $upr_fetch_req_from = get_option( 'upr_prayer_fetch_req_from' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Which Requests Would You like to Display?', 'prayers' ); ?></label></td>
					<td>
					<select name="upr_prayer_fetch_req_from" class="form-control">
						<option value="all"><?php _e( 'all of them', 'prayers' ); ?></option>
						<option value="14" 
						<?php
						if ( $upr_fetch_req_from == 14 ) {
							echo 'selected';}
						?>
						><?php _e( 'only the last 14 days', 'prayers' ); ?></option>
						<option value="30" 
						<?php
						if ( $upr_fetch_req_from == 30 ) {
							echo 'selected';}
						?>
						><?php _e( 'only the last 30 days', 'prayers' ); ?></option>
						<option value="60" 
						<?php
						if ( $upr_fetch_req_from == 60 ) {
							echo 'selected';}
						?>
						><?php _e( 'only the last 60 days', 'prayers' ); ?></option>
						<option value="90" 
						<?php
						if ( $upr_fetch_req_from == 90 ) {
							echo 'selected';}
						?>
						><?php _e( 'only the last 90 days', 'prayers' ); ?></option>
						<option value="120" 
						<?php
						if ( $upr_fetch_req_from == 120 ) {
							echo 'selected';}
						?>
						><?php _e( 'only the last 120 days', 'prayers' ); ?></option>
					</select>
					<p><?php _e( 'Choose how many prayers you want to display.', 'prayers' ); ?></p></td>
				</tr>
				
				<?php $upr_disable_ip_logging = get_option( 'upr_disable_ip_logging' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Disable IP logging', 'prayers' ); ?></label></td>
					<td><input name="upr_disable_ip_logging" id="upr_disable_ip_logging" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_disable_ip_logging == 1 ) {
						echo 'checked';
                    }
					?>
					> <?php _e( 'Select this to disable IP logging, IP addresses then set to 127.0.0.1', 'prayers' ); ?></td>
				</tr> 
                
				<?php $upr_use_local_emojis = get_option( 'upr_use_local_emojis' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Use local emojis', 'prayers' ); ?></label></td>
					<td><input name="upr_use_local_emojis" id="upr_use_local_emojis" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_use_local_emojis == 1 ) {
						echo 'checked';
                    }
					?>
					> <?php _e( 'Then you can disable emoji in WP.', 'prayers' ); ?></td>
				</tr>                 

				<?php $upr_disable_hide_all_prayed = get_option( 'upr_disable_hide_all_prayed' ); ?>
				<tr>
					<td scope="row"><label><?php _e( 'Disable function "hide all prayed"', 'prayers' ); ?></label></td>
					<td><input name="upr_disable_hide_all_prayed" id="upr_disable_hide_all_prayed" value="1" class="regular-text" type="checkbox" 
					<?php
					if ( $upr_disable_hide_all_prayed == 1 ) {
						echo 'checked';
                    }
					?>
					> <?php _e( 'This hides the "hide all prayed" button and disables the prayer_requests cookie.', 'prayers' ); ?></td>
				</tr>    
                <?php $upr_accept_privacy = get_option( 'upr_accept_privacy' ); ?>
				<tr>
					<td><label><?php _e( 'Accept privacy text', 'prayers' ); ?></label></td>
					<td><textarea name="upr_accept_privacy" id="upr_accept_privacy"><?php echo $upr_accept_privacy; ?></textarea>
                    <?php _e( 'If this is not empty this will be shown next to a checkbox.', 'prayers' ); ?>
                    </td>
				</tr>

			</tbody>
		</table>
		<p class="submit"><input name="prayerssettings" id="submit" class="button button-primary" value="<?php _e( 'Update', 'prayers' ); ?>" type="submit"></p>
	  </form>
</div>
