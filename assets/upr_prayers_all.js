// JavaScript Document
jQuery( document ).ready(
	function($) {
		$( "#prayer-js-save-item-data" ).click(
			function(){
				$( ".help-block" ).hide();
				if ($( "#prayer_author_name" ).val() == '') {
					$( "#prayer_author_name" ).focus();
					$( "#prayer_author_name_error" ).show();
					return false;
				}
				/*if($("#prayer_author_email").val()==''){
				$("#prayer_author_email").focus();
				$("#prayer_author_email_error").show();
				return false;
				}*/
				if ($( "#prayer_title" ).val() == '') {
					$( "#prayer_title" ).focus();
					$( "#prayer_title_error" ).show();
					return false;
				}
				if ($( "#prayer_messages" ).val() == '') {
					$( "#prayer_messages" ).focus();
					$( "#prayer_messages_error" ).show();
					return false;
				}
			}
		);
		$( '.prayer-js-show-hide' ).click(
			function(){
				var ID = $( this ).attr( 'id' );
				$( '#prayer-js-commentlist-' + ID ).slideToggle( 'slow' );
			}
		);
		$( '.prayer-js-cancel-comment' ).click(
			function(){
				var id = $( this ).attr( 'id' );
				$( '#prayer-js-reply-' + id ).hide();
			}
		);
		$( '.prayer-js-pray-replay' ).click(
			function(){
				var id = $( this ).attr( 'id' );
				$( '#prayer-js-reply-' + id ).show();
			}
		);
		$( document ).on(
			"click",
			'.prayer-js-comment-reply-link',
			function(e) {
				var commentlist = $( this ).closest( '.prayer-js-commentlist' ).attr( 'id' );
				var prayer_id   = commentlist.replace( 'prayer-js-commentlist-', "" );
				$( '#comment_post_ID' ).val( prayer_id );
			}
		);
		$( document ).on(
			"click",
			'.prayer-js-commentlist #submit',
			function(event) {
				var logged_in = $( '#logged_in' ).val();
				event.preventDefault();
				// $(this).closest('.form-submit').html('Wait...');
				var form                 = $( this ).closest( 'form' ).attr( 'id' );
				var url                  = $( '#current-page' ).val();
				var prayer_id            = $( this ).closest( ".comment-form" ).find( "#comment_post_ID" ).val();
				var comment_parent       = $( this ).closest( ".comment-form" ).find( "#comment_parent" ).val();
				var pray_reply           = $( this ).closest( ".comment-form" ).find( "#comment" ).val();
				var comment_author       = $( this ).closest( ".comment-form" ).find( "#prayer-js-author" ).val();
				var comment_author_email = $( this ).closest( ".comment-form" ).find( "#prayer-js-email" ).val();
				var comment_author_url   = $( this ).closest( ".comment-form" ).find( "#prayer-js-url" ).val();
				var ajax_url             = $( "#admin-ajax" ).val();
				$.ajax(
					{
						url : ajax_url,
						type : 'post',
						data : {
							action : 'ajax_pray_response',
							prayer_id : prayer_id,
							comment_parent : comment_parent,
							pray_reply: pray_reply,
							comment_author: comment_author,
							comment_author_email: comment_author_email,
							comment_author_url: comment_author_url,
						},
						success : function( response ) {
							$( '#prayer-js-commentlist-' + prayer_id ).slideToggle( 'slow' );
							window.location.href = url + '/#prayer-js-commentlist-' + prayer_id;
						}
					}
				);
			}
		);
		$( document ).on(
			"click",
			'.prayer-js-prayresponse',
			function(event) {              
				var url                  = $( '#prayer-js-current-url' ).val();
				var prayer_id            = $( this ).attr( 'id' );
                
                // privacy
                if ($("input[name='prayer-js-acceptprivacy-"+prayer_id+"']").val()) { // exists?
                    if (!$("input[name='prayer-js-acceptprivacy-"+prayer_id+"']").is(':checked')) {
                        alert(prayerjstranslation["acceptprivacy"]);
                        return false;
                    } // if
                } // if
                
				var pray_reply           = $( "#prayer-js-pray-reply-" + prayer_id ).val();
				var comment_author       = $( "#prayer-js-author-" + prayer_id ).val();
				var comment_author_email = $( "#prayer-js-email-" + prayer_id ).val();
				var comment_author_url   = $( "#prayer-js-url-" + prayer_id ).val();
				var	comment_parent = 0;
				if (pray_reply == '') {
					$( "#prayer-js-pray-reply-" + prayer_id ).focus();
					return false;
				}
				if (comment_author == '') {
					$( "#prayer-js-author-" + prayer_id ).focus();
					return false;
				}
				if (comment_author_email == '') {
					$( "#prayer-js-email-" + prayer_id ).focus();
					return false;
				}
                $( this ).val( 'Wait..' );
				var ajax_url       = $( "#admin-ajax" ).val();
				$.ajax(
					{
						url : ajax_url,
						type : 'post',
						data : {
							action : 'ajax_pray_response',
							prayer_id : prayer_id,
							comment_parent : comment_parent,
							pray_reply: pray_reply,
							comment_author: comment_author,
							comment_author_email: comment_author_email,
							comment_author_url: comment_author_url
						},
						success : function( response ) {
							$( '#prayer-js-commentlist-' + prayer_id ).slideToggle( 'slow' );
                            alert( prayerjstranslation["commentsavedmessage"] );
							window.location.href = url + '/#prayer-js-commentlist-' + prayer_id;
						}
					}
				);
			}
		);
        // todo answer to a comment is saved wrong/showed invalid relation, sometimes
		$( document ).on(
			"click",
			'.prayer-js-prayresponsreply',
			function(event) {
				var url                  = $( "#prayer-js-current-url" ).val();
				var prayer_id            = $( this ).attr( 'id' );

                // privacy
                if ($("input[name='prayer-js-acceptprivacy-"+prayer_id+"']").val()) { // exists?
                    if (!$("input[name='prayer-js-acceptprivacy-"+prayer_id+"']").is(':checked')) {
                        alert(prayerjstranslation["acceptprivacy"]);
                        return false;
                    } // if
                } // if

				var pray_reply           = $( "#prayer-js-pray-reply-" + prayer_id ).val();
				var comment_author       = $( "#prayer-js-author-" + prayer_id ).val();
				var comment_author_email = $( "#prayer-js-email-" + prayer_id ).val();
				var comment_author_url   = $( "#prayer-js-url-" + prayer_id ).val();
				var	comment_post_ID      = $( "#comment_post_ID_" + prayer_id ).val();
				var	comment_parent       = $( "#comment_parent_" + prayer_id ).val();
				if (pray_reply == '') {
					$( "#prayer-js-pray-reply-" + prayer_id ).focus();
					return false;
				}
				if (comment_author == '') {
					$( "#prayer-js-author-" + prayer_id ).focus();
					return false;
				}
				if (comment_author_email == '') {
					$( "#prayer-js-email-" + prayer_id ).focus();
					return false;
				}
				$( this ).val( 'Wait..' );
				var ajax_url = $( "#admin-ajax" ).val();
				$.ajax(
					{
						url : ajax_url,
						type : 'post',
						data : {
							action : 'ajax_pray_response',
							prayer_id : comment_post_ID,
							comment_parent : comment_parent,
							pray_reply: pray_reply,
							comment_author: comment_author,
							comment_author_email: comment_author_email,
							comment_author_url: comment_author_url
						},
						success : function( response ) {
							$( '#prayer-js-commentlist-' + comment_post_ID ).slideToggle( 'slow' );
                            alert( prayerjstranslation["commentsavedmessage"] );
							window.location.href = url + '/#prayer-js-commentlist-' + comment_post_ID;
						}
					}
				);
			}
		);
	}
);
function refreshCaptcha(){
	var img = document.images['captchaimg'];
	img.src = img.src.substring( 0,img.src.lastIndexOf( "?" ) ) + "?rand=" + Math.random() * 1000;
}
function do_pray(pray_id,time_interval,user_id){
    jQuery( '#prayer-js-do-pray-' + pray_id ).attr( 'disabled','disabled' );
    jQuery( '#prayer-js-prayer-loading-icon-' + pray_id ).show();
    
	var ajax_url = jQuery( "#admin-ajax" ).val();
	jQuery.ajax(
		{
			url : ajax_url,
			type : 'post',
			data: {
				action : 'ajax_do_pray',
				prayer_id : pray_id
			},
			beforeSend: function() {
				jQuery( '#prayer-js-do-pray-' + pray_id ).attr( 'disabled','disabled' );
				jQuery( '#prayer-js-do-pray-' + pray_id ).addClass( 'prayed' );
			},
			success: function(data) {
				if (data == "already") {
                    jQuery( '#prayer-js-do-pray-' + pray_id ).val( jQuery( '#prayer-js-do-pray-' + pray_id ).val() + "already!" );
                }
				if (data == "Prayed") {
                    jQuery( '#prayer-js-prayer-loading-icon-' + pray_id ).hide();
					var count = jQuery( '#prayer-js-prayer-count-' + pray_id ).html();
					count     = parseInt( count ) + 1;
					jQuery( '#prayer-js-do-pray-' + pray_id ).val( jQuery( '#prayer-js-do-pray-' + pray_id ).val() + "✓" );
                    // show checked hook after prayed click
					jQuery( '#prayer-js-prayer-check-field-' + pray_id ).html( "&#10003;" );
					jQuery( '#prayer-js-prayer-count-' + pray_id ).html( count );
					jQuery( '#prayer-js-do-pray-' + pray_id ).attr( 'disabled' );
					time_interval = time_interval * 1000;
					setTimeout(
						function(){
							//jQuery( '#prayer-js-do-pray-' + pray_id ).removeAttr( 'disabled' );
                            /*
							if (data == "Prayed") {
                                // if not type prophecy liked!
                                type = jQuery(this).attr("data-type");
                                if (type == "prayers") {
                                    jQuery( '#prayer-js-do-pray-' + pray_id ).val( 'Prayed' );
                                } else {
                                    jQuery( '#prayer-js-do-pray-' + pray_id ).val( 'Liked' );
                                }
                                // todo disable this button
							} else {
								jQuery( '#prayer-js-do-pray-' + pray_id ).val( 'Failure' );
							}*/
						},
						time_interval
					);
				}
			}
		}
	);
}
