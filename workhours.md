| Worker   | Type       |     Date    | Minutes |
|----------|:----------:|:----------:|--------:|
| jkmssoft | php	| 2020-11-06  | 60 |
| jkmssoft | css/js	| 2020-11-09  | 60 |
| jkmssoft | php/css/js	| 2020-11-10  | 120 |
| jkmssoft | translation	| 2020-11-12  | 30 |
| jkmssoft | css	| 2020-11-16  | 30 |
| jkmssoft | html/css/php fix design, cookie	| 2020-11-20  | 90 |
| jkmssoft | fix html, css	| 2020-12-04  | 60 |
| jkmssoft | rest api	| 2020-12-04  | 60 |
| jkmssoft | rest api	| 2020-12-05  | 90 |
| jkmssoft | fix show all	| 2020-12-13  | 10 |
| jkmssoft | js loading icon, pray now text	| 2020-12-23  | 15 |
| jkmssoft | js ging nicht mehr: fix div statt span!	| 2021-01-03  | 30 |
| jkmssoft | disable cookie, disable remote emojis | 2021-03-24  | 120 |
| jkmssoft | add accept privacy (submit: js + php, comments: js only) | 2021-03-25  | 60 |
