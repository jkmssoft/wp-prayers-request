		<form id="form_gc" method="post" action="#gotomessage_<?=$type?>">
			<div class="wpgmp-frontend lxt-kewal">
				<?php
				$showform                 = false;
				$upr_show_prayer_category = get_option( 'upr_show_prayer_category' );
				$upr_prayer_show_country  = get_option( 'upr_prayer_show_country' );
				$upr_prayer_hide_captcha  = get_option( 'upr_prayer_hide_captcha' );
				if ( ! isset( $upr_prayer_hide_captcha ) || $upr_prayer_hide_captcha == '' ) {
					$upr_prayer_hide_captcha = 0;
				}
				$prayer_heading             = get_option( 'upr_prayer_list_title' );
				$upr_prayer_spam_prevention = get_option( 'upr_prayer_spam_prevention' );
				if ( empty( $prayer_heading ) ) {
					$prayer_heading = '';
				}
                echo '<div id="gotomessage_' . $type . '"></div>';
                if ( $type == 'prophecies' ) {
                    _e( 'GOD told you something? Share it here! Spam protection: it will not be displayed directly, but an editor will publish it in a short time.', 'prayers' );
                } elseif ( $type == 'witnesses' ) {
                    // todo important
                    _e( 'You want to give witness? Here you can!', 'prayers' );
                } elseif ( $type == 'prayers' ) {
                    _e( 'If you need prayer, you can simply fill out the following form. Then your prayer request will be displayed on the website and others can pray for your request (spam protection: it will not be displayed directly, but an editor will publish it in a short time).', 'prayers' );
                    echo '<br><b>' . __( 'If you want to say thank you you can simply comment your prayer request.', 'prayers' ) . '</b>';
                }
				
				$current_url = get_permalink( get_the_ID() );
				if ( isset( $message ) ) {
					echo $message;
				}
				$upr_login_not_required_request = get_option( 'upr_login_not_required_request' );
				if ( $upr_login_not_required_request != 1 ) {
					if ( is_user_logged_in() ) {
						$showform = true;
					}
				} else {
					$showform = true;
				}
				if ( $showform ) {
					?>
					<div class="form-group ">
						<div class="col-md-3"><label for="prayer_author_name">
						<?php
						_e(
							'Name',
							'prayers'
						);
						?>
																				</label><span class="inline-star">*</span></div>
						<div class="col-md-8"><input name="prayer_author_name" id="prayer_author_name"
													 class="form-control" type="text" value="">
							<p class="help-block" id="prayer_author_name_error">
							<?php
							_e(
								'This field is required',
								'prayers'
							)
							?>
																				</p></div>
					</div>
					<div class="form-group ">
						<div class="col-md-3"><label for="prayer_author_email">
						<?php
						_e(
							'Email',
							'prayers'
						);
						?>
																				</label><?php /*<span class="inline-star">*</span>*/ ?></div>
						<div class="col-md-8"><input name="prayer_author_email" id="prayer_author_email"
													 class="form-control" type="email" value="">
							<?php
							/*
							<p class="help-block" id="prayer_author_email_error"><?php _e('This field is required',
									'prayers') ?></p>*/
							?>
							</div>
					</div>
					<?php if ( $upr_show_prayer_category == 1 ) : ?>
					<div class="form-group ">
						<?php
						$args = array(
							'option_none_value' => '-1',
							'orderby'           => 'ID',
							'order'             => 'ASC',
							'show_count'        => 0,
							'hide_empty'        => 0,
							'child_of'          => 0,
							'echo'              => 1,
							'selected'          => 0,
							'hierarchical'      => 0,
							'name'              => 'prayer_author_category',
							'depth'             => 0,
							'tab_index'         => 0,
							'taxonomy'          => 'prayertypes',
							'hide_if_empty'     => false,
							'value_field'       => 'term_id',
						);
						?>
						<div class="col-md-3"><label for="prayer_author_category">
						<?php
						_e(
							'Category',
							'prayers'
						)
						?>
																					</label></div>
						<div class="col-md-8"><?php wp_dropdown_categories( $args ); ?></div>
					</div>
						<?php
				endif;
					if ( $upr_prayer_show_country == 1 ) :
						?>
					<div class="form-group ">
						<div class="col-md-3"><label for="prayer_author_country">
						<?php
						_e(
							'Country',
							'prayers'
						)
						?>
																					</label></div>
						<div class="col-md-8">
							<select name="prayer_author_country" class="form-control">
									<?php
									foreach ( $this->select_country as $key => $val ) {
										echo '<option value="' . $val . '">' . __( $val, 'prayers' ) . '</option>';
									}
									?>
							</select>
						</div>
					</div>
					<?php endif; ?>
					<div class="form-group">
						<div class="col-md-3"><label for="prayer_title"><?php _e( 'Title', 'prayers' ); ?></label><span
									class="inline-star">*</span></div>
						<div class="col-md-8"><input name="prayer_title" id="prayer_title" class="form-control"
													 type="text" value="">
							<p class="help-block" id="prayer_title_error">
							<?php
							_e(
								'This field is required',
								'prayers'
							)
							?>
																			</p></div>
					</div>
					<div class="form-group ">
						<div class="col-md-3"><label for="prayer_messages">
						<?php
						_e(
							'Message',
							'prayers'
						)
						?>
																			</label><span class="inline-star">*</span></div>
						<div class="col-md-8"><textarea rows="5" name="prayer_messages" id="prayer_messages"
														class="form-control"></textarea>
							<p class="help-block" id="prayer_messages_error">
							<?php
							_e(
								'This field is required',
								'prayers'
							)
							?>
																				</p></div>
					</div>
					<?php if ( $upr_prayer_spam_prevention == 1 ) : ?>
					<label for="is_humans" class="is_humans">
						<?php
						_e(
							'Human check: Leave this field empty',
							'prayers'
						)
						?>
																</label>
				<input type="text" name="is_humans" id="is_humans" class="is_humans"/>
				<?php endif; ?>
					<?php if ( $upr_prayer_hide_captcha == 0 ) : ?>
					<!--<div class="form-group ">-->
					<!--   	<div class="col-md-3"><label for="prayer_captcha">
						<?php
						_e(
							'Captcha',
							'prayers'
						)
						?>
																				</label><span class="inline-star">*</span></div>-->
					<!--       <div class="col-md-8">-->
					<!--           <div class="col-md-3">-->
					<!--           <img src="<?php echo plugin_dir_url( __FILE__ ) . '/captcha.php?rand=' . rand(); ?>" id="captchaimg"></div>-->
					<!--           <div class="col-md-9">-->
					<!--           <input type="text" name="captcha_code" id="captcha_code" class="form-control" autocomplete="off" /></div>-->
					<!--           <p class="help-block" id="captcha_code_error"></p>                    -->
					<!--       </div>                -->
					<!--</div>-->
					<!--	<div class="form-group ">-->
					<!--	<div class="col-md-3">&nbsp;</div>-->
					<!--    <div class="col-md-8">-->
					<!--    	<p class="help-block">
						<?php
						_e(
							"Enter here captcha. Can't read the image?",
							'prayers'
						)
						?>
														 <?php
															_e(
																'Click',
																'prayers'
															)
															?>
															 <a href="javascript: refreshCaptcha();">
															<?php
															_e(
																'HERE',
																'prayers'
															)
															?>
																		</a> <?php _e( 'to refresh', 'prayers' ); ?>.</p>            	</div>-->
					<!--</div> -->
				<?php endif; ?>
					<?php
					$upr_prayer_sitekey = get_option( 'upr_prayer_sitekey' );
					?>
					<?php if ( $upr_prayer_hide_captcha == 1 ) : ?>
					<script src='https://www.google.com/recaptcha/api.js'></script>
					<div class="form-group ">
					   
						<div class="col-md-8">
							<div class="g-recaptcha col-md-6" data-sitekey="<?php echo $upr_prayer_sitekey; ?>"></div>
						</div>
					</div>
				<?php endif; ?>
					<div class="form-group ">
						<div class="col-md-12">
                            <?php
                            if ( $type == 'prophecies' ) : ?>
                                <input name="save_prophecy_data" id="save_prayer_data"
                            <?php elseif ( $type == 'witnesses' ) : ?>
                                <input name="save_witnesses_data" id="save_prayer_data"
                            <?php elseif ( $type == 'prayers' ) : ?>
                                <input name="save_prayer_data" id="save_prayer_data"
                            <?php endif ?>
								class="btn btn-primary" value="<?php _e( 'Submit', 'prayers' ); ?>" type="submit">
						</div>
					</div>
				<?php } else { ?>
					<p><?php _e( 'Sorry, you have to login first to request a prayer.', 'prayers' ); ?>
						. <?php _e( 'Click to', 'prayers' ); ?> <a href="<?php echo wp_login_url( $current_url ); ?>"><?php _e( 'login', 'prayers' ); ?></a>.
					</p>
				<?php } ?>
			</div>
		</form>
        <?php
        if ( $upr_prayer_hide_captcha == 1 ) : ?>
		<script>
			jQuery(document).ready(function ($) {
				$('#form_gc').submit(function (event) {
					//event.preventDefault();
					var googleResponse = jQuery('#g-recaptcha-response').val();
					if (!googleResponse) {
						alert("Please verify reCaptcha.");
						return false;
					} else {
						return true;
					}
				});
			});
		</script>
	<?php endif; ?>
