<?php
$comments = get_comments(
	array(
		'post_id' => $wp_prayer_id,
		'status'  => 'approve',
	)
);

$html .= '<li>';
$html .= '<div>';
    $html .= '<div>';
        $html .= '<div class="prayer-css-left" style="font-size: 14px;">';
            // type
            $html .= $typeIcons[$prayer->post_type].' ';
            if ( $prayer->post_type == 'prayers') {
                $html .= __( 'Request', 'prayers' );
            } elseif ( $prayer->post_type == 'prophecies') {
                $html .= __( 'Impression', 'prayers' );
            } elseif ( $prayer->post_type == 'witnesses') {
                $html .= __( 'Hearing', 'prayers' );
            }
        $html .= '</div>';
        $html .= '<div class="prayer-css-right" style="font-size: 12px;">' . $prayer_date . '</div>';
    $html .= '</div>';
    $html .= '<br><h4 class="prayer-css-title"><a rel=“nofollow” href="' . $wp_prayer_permalink . '">' . $wp_prayer_title . '</a></h4>';
    

    $wp_prayer_content = nl2br( $wp_prayer_content, false );
    $wp_prayer_content_stripped = strip_tags($wp_prayer_content, '<br>');
    $html .= 
        '<div>
        <div id="prayer-js-prayer-content-less-' . $wp_prayer_id . '">'
        . (strlen($wp_prayer_content_stripped) > 150 ? substr(strip_tags($wp_prayer_content_stripped), 0, 150) . '...' : $wp_prayer_content_stripped)
        . '</div>';
    if ( strlen($wp_prayer_content_stripped) > 150 ) {
        $html .= 
            '<div style="display: none;" id="prayer_content_more_' . $wp_prayer_id . '">'
            . $wp_prayer_content_stripped
            . '</div>'
            . '<div>
            <span id="prayer-js-show-more-' . $wp_prayer_id . '" class="prayer-css-pointer prayer-js-toggle-more" data-item-id="'. $wp_prayer_id . '">'
            . __( 'Show more', 'prayers' ) . '▼</span>'
            . '<span style="display: none;" id="prayer-js-show-less-' . $wp_prayer_id . '" class="prayer-css-pointer prayer-js-toggle-more" data-item-id="'. $wp_prayer_id . '">'
            . __( 'Show less', 'prayers' ) . '▲</span>'
            . '</div>';
    }
    $html .= '</div><div>';
    $html .= '<div class="prayer-css-left">';
        $prayText = __( 'Pray Now', 'prayers' );
        if ( $prayer->post_type == 'prophecies' ) {
            $prayText = __( 'Amen', 'prayers' );
        } elseif ( $prayer->post_type == 'witnesses' ) {
            $prayText = __( 'Hallelujah', 'prayers' );
        }
        $upr_use_local_emojis = get_option('upr_use_local_emojis');
        $html .= '<button name="do_pray" style="background: transparent;" class="prayer-css-pray-button" 
                data-type="' . $prayer->post_type. '" id="prayer-js-do-pray-' . $wp_prayer_id . '" 
                onclick="do_pray(' . $wp_prayer_id . ',' . $upr_time_interval_pray_prayed_button . ',' . $uid . ');" 
                title="' . __( 'Pray', 'prayers' ) . '" value="' . strip_tags($typeIcons['prayers']) . '" type="submit">
                ' . ($upr_use_local_emojis == 1 ? $typeIcons['prayers'] : strip_tags($typeIcons['prayers'])) . ' ' . $prayText . '
                </button>
                <span style="display: none;" id="prayer-js-prayer-loading-icon-' . $wp_prayer_id . '"><img src="' . plugins_url( '../assets/images/ajax-loader.gif', __FILE__ ) . '" alt="loading icon"></span> 
                <span id="prayer-js-prayer-check-field-' . $wp_prayer_id . '"></span>
                <span id="prayer-js-prayer-count-' . $wp_prayer_id . '">' . $wp_prayers_count . '</span>';
        // comment:
        $html .= '<span style="margin-left: 10px;">'.($upr_use_local_emojis == 1 ? $typeIcons['comment'] : '&#128172') . ' ' . get_comments_number( $wp_prayer_id ) . '</span>'; // todo count
    $html .= '</div>';
    $html .= '<div class="prayer-css-right">';
        if ( $upr_allow_comments_prayer_request == 1 ) {
            $html .= '<div class="reply">';
            if ( $comments ) {
                $html .= '<a style="cursor: pointer; text-align:right; width:100%; font-size: 12px;" id="' . $wp_prayer_id . '" style="cursor: pointer;" class="prayer-js-show-hide">' . __(
                    'Show/Hide Comments',
                    'prayers'
                ) . '</a>&nbsp;';
            }
            if ( comments_open( $wp_prayer_id ) ) {
                if ( $registration == 1 ) {
                    $html .= '<a href="' . wp_login_url( $current_url ) . '">' . __(
                        'Log in to Reply',
                        'prayers'
                    ) . '</a>';
                } else {
                    $html .= '<a style="font-size: 12px; cursor: pointer;" class="prayer-js-comment-reply-link prayer-js-pray-replay" id="' . $wp_prayer_id . '">' . __(
                        'Reply',
                        'prayers'
                    ) . '</a>';
                }
            }
            $html .= '</div>';
        }
    $html .= '</div>';
$html .= '</div>';

// todo form as child of span not allowed, mv to div - problems with css?
$html .= '<span id="prayer-js-reply-' . $wp_prayer_id . '" style="display:none">
		<form action="" method="post">
		<br><label for="author">' . __(
	'Comment',
	'prayers'
) . ' <a id="' . $wp_prayer_id . '" class="prayer-js-cancel-comment prayer-css-pointer">' . __( 'Cancel', 'prayers' ) . '</a></label>
		<p><textarea cols="40" name="pray_reply" id="prayer-js-pray-reply-' . $wp_prayer_id . '"></textarea></p>
		<input id="prayer_id" name="prayer_id" value="' . $wp_prayer_id . '" type="hidden">';
if ( ! is_user_logged_in() ) {
	$html .= '<p class="comment-form-author"><label for="prayer-js-author-' . $wp_prayer_id . '">' . __(
		'Name',
		'prayers'
	) . '</label>
        <input id="prayer-js-author-' . $wp_prayer_id . '" name="author" type="text" value="" size="30"></p>';
    /*   
        <label for="prayer-js-email-' . $wp_prayer_id . '">' . __(
		'Email',
		'prayers'
	) . ' </label><input id="prayer-js-email-' . $wp_prayer_id . '" name="email" type="text" value="" size="30">
                </p>
		<p></p><label for="prayer-js-url-' . $wp_prayer_id . '">' . __(
		'Website',
		'prayers'
	) . '</label><input id="prayer-js-url-' . $wp_prayer_id . '" name="url" type="text" value="" size="30">
                </p>';*/
}

$upr_accept_privacy = get_option( 'upr_accept_privacy' ); 
if ( $upr_accept_privacy != '' ) {
    $html .= '
    <p class="comment-form-accept-privacy">
        <input type="checkbox" name="prayer-js-acceptprivacy-' . $wp_prayer_id . '" id="prayer-label-acceptprivacy-' . $wp_prayer_id . '" value="1">
        <label for="prayer-label-acceptprivacy-' . $wp_prayer_id . '">' . $upr_accept_privacy . '</label>
    </p>';
}

$html    .= '<input class="prayer-js-prayresponse" id="' . $wp_prayer_id . '" name="pray_response" value="' . __(
	'Send',
	'prayers'
) . '" type="button"></form></span>';

if ( $comments ) {
	ob_start();
	wp_list_comments(
		array(
'reverse_top_level' => false,
'callback'          => 'upr_all_comments',
		),
		$comments
	);
	$variable = ob_get_clean();
	$html    .= '<ol class="prayer-js-commentlist" id="prayer-js-commentlist-' . $wp_prayer_id . '">' . $variable . '</ol>';
}

$html .= '</li>';
