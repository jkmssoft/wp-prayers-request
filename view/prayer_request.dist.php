<?php
            $html .= '<li>';
			$html .= '<div class="wsl_prayer_left">';
			$html .= '<h5><a href="' . $wp_prayer_permalink . '">' . $wp_prayer_title . '</a></h5>';
			$html .= $wp_prayer_content;
			$html .= '<div class="wsl_prayer_right" style="margin-top:10px;margin-bottom:10px;">';
			if ( $upr_hide_prayer_button != 1 ) {
				if ( $upr_show_do_pray ) :
					/*
					if(count($prayer_performed)>0){
						$html .= '<input name="do_pray" class="prayed" id="do_pray_'.$wp_prayer_id.'" value="'.__('Prayed','prayers').'" type="submit">';
					} else {
						$html .= '<input name="do_pray" class="prayed" id="do_pray_'.$wp_prayer_id.'" onclick="do_pray('.$wp_prayer_id.','.$upr_time_interval_pray_prayed_button.','.$uid.');"
						value="'.__('Pray','prayers').'" type="submit">';
					}*/
					$html .= '<input name="do_pray" class="prayed" id="do_pray_' . $wp_prayer_id . '" onclick="do_pray(' . $wp_prayer_id . ',' . $upr_time_interval_pray_prayed_button . ',' . $uid . ');" 
							value="' . __( 'Pray', 'prayers' ) . '" type="submit">';
				endif;
			}
			$html .= '</div><!-- wsl_prayer_right-->';
			if ( $upr_display_username == 1 ) {
				$author_display_name = '';
				$submittedby         = '';
				$author_id           = $prayer->post_author;
				if ( $author_id > 0 ) {
					$post_author         = get_user_by( 'ID', $author_id );
					$author_display_name = $post_author->display_name . ' ';
					$submittedby         = __( 'Submitted by', 'prayers' ) . ' ' . ucwords( $author_display_name ) . __(
						'on',
						'prayers'
					) . ' ';
				} else {
					$author_display_name = get_post_meta( $wp_prayer_id, 'prayers_name', true );
					$author_display_name = $author_display_name . ' ';
					$submittedby         = __( 'Submitted by', 'prayers' ) . ' ' . ucwords( $author_display_name ) . __(
						'on',
						'prayers'
					) . ' ';
				}
				$html .= '<div class="postmeta">' . $submittedby . __( $prayer_date ) . '<br>';
			} else {
				$html .= '<div class="postmeta">' . __( $prayer_date ) . '<br>';
			}
			if ( $upr_hide_prayer_count == 0 ) {
				$html .= '<span id="prayer_count_' . $wp_prayer_id . '">' . $wp_prayers_count . '</span> ' . __( 'time(s)', 'prayers' )
					. ' ' . __( 'Prayed', 'prayers' );
			}
			$html .= '</div> <!-- wsl_prayer_left -->';
			$html .= '</div>';
			if ( $upr_allow_comments_prayer_request == 1 ) {
				if ( comments_open( $wp_prayer_id ) ) {
					if ( is_user_logged_in() ) {
						$html .= '<div class="reply"><a class="comment-reply-link pray-replay" id="' . $wp_prayer_id . '">' . __(
							'Reply',
							'prayers'
						) . '</a></div>';
					} else {
						if ( $registration == 1 ) :
							$html .= '<div class="reply"><a href="' . wp_login_url( $current_url ) . '">' . __(
								'Log in to Reply',
								'prayers'
							) . '</a></div>';
						else :
							$html .= '<div class="reply"><a class="comment-reply-link pray-replay" id="' . $wp_prayer_id . '">' . __(
								'Reply',
								'prayers'
							) . '</a></div>';
						endif;
					}
				}
			}
            // todo form as child of span not allowed, mv to div - problems with css?
			$html .= '<span id="reply_' . $wp_prayer_id . '" style="display:none">
					<form action="" method="post">
					<label for="author">' . __(
				'Comment',
				'prayers'
			) . ' <a id="' . $wp_prayer_id . '" class="cancelcomment">' . __( 'Cancel', 'prayers' ) . '</a></label>
					<p><textarea name="pray_reply" id="pray_reply_' . $wp_prayer_id . '"></textarea></p>
					<input id="prayer_id" name="prayer_id" value="' . $wp_prayer_id . '" type="hidden">';
			if ( ! is_user_logged_in() ) {
				$html .= '<p class="comment-form-author"><label for="author">' . __(
					'Name',
					'prayers'
				) . '</label><input id="author_' . $wp_prayer_id . '" name="author" type="text" value="" size="30"></p><label for="author">' . __(
					'Email',
					'prayers'
				) . ' </label><input id="email_' . $wp_prayer_id . '" name="email" type="text" value="" size="30">
                </p>
					<p></p><label for="author">' . __(
					'Website',
					'prayers'
				) . '</label><input id="url_' . $wp_prayer_id . '" name="url" type="text" value="" size="30">
                </p>';
			}
			$html    .= '<input class="prayresponse" id="' . $wp_prayer_id . '" name="pray_response" value="' . __(
				'Send',
				'prayers'
			) . '" type="button"></form></span>';
			$comments = get_comments(
				array(
					'post_id' => $wp_prayer_id,
					'status'  => 'approve',
				)
			);
			if ( $comments ) {
				$html .= '<div style=" text-align:right; width:100%;"><a id="' . $wp_prayer_id . '" class="show_hide">' . __(
					'Show/Hide',
					'prayers'
				) . '</a></div>';
				ob_start();
				wp_list_comments(
					array(
						'reverse_top_level' => false,
						'callback'          => 'upr_comments',
					),
					$comments
				);
				$variable = ob_get_clean();
				$html    .= '<ol class="commentlist" id="commentlist_' . $wp_prayer_id . '">' . $variable . '</ol>';
			}

			$html .= '</li>';
