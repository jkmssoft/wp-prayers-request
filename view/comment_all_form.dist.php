<?php
    // echo "<pre>"; print_r($comment);
	$upr_allow_comments_prayer_request = get_option( 'upr_allow_comments_prayer_request' );
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
    <<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID(); ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
		<?php
		if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
		}
		?>
		<?php
		printf(
			__( '<cite class="fn">%s</cite> <span class="says">' . __( 'says', 'prayers' ) . ':</span>' ),
			// jkmssoft: hide author name if !upr_display_username_on_prayer_listing
			get_option( 'upr_display_username_on_prayer_listing' ) ? get_comment_author_link() : __( 'name hidden', 'prayers' )
		);
		?>
	</div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
		<br/>
	<?php endif; ?>

	<?php comment_text(); ?>
	<div class="comment-meta commentmetadata" style="font-size: 12px;">
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
		<?php
		/* translators: 1: date, 2: time */
		printf(
			__( '%1$s at %2$s' ),
			get_comment_date(),
			get_comment_time()
		);
		?>
		</a>
		<?php
		edit_comment_link( __( '(Edit)' ), '  ', '' );
		?>
	</div>
	<?php
	$registration = get_option( 'comment_registration' );
	if ( $upr_allow_comments_prayer_request == 1 ) {
		if ( comments_open( $comment->comment_post_ID ) ) {
			if ( is_user_logged_in() ) {
				echo '<div class="reply">
                    <a style="cursor: pointer;font-size: 12px; " class="prayer-js-comment-reply-link prayer-js-pray-replay" id="' . $comment->comment_ID . '">' . __(
					'Reply',
					'prayers'
				) . '</a></div>';
			} else {
				if ( $registration == 1 ) :
					echo '<div class="reply">
                        <a href="' . wp_login_url( $current_url ) . '" id="' . $current_url . '">' . __(
						'Log in to Reply',
						'prayers'
					) . '</a></div>';
				else :
					echo '<div class="reply">
                        <a style="cursor: pointer;font-size: 12px; " class="prayer-js-comment-reply-link prayer-js-pray-replay" id="' . $comment->comment_ID . '">' . __(
						'Reply',
						'prayers'
					) . '</a></div>';
				endif;
			}
		}
	}
    ?>
    <div id="prayer-js-reply-<?php comment_ID(); ?>" class="comment-respond" style="display:none">
		<form id="prayer-js-commentform" action="" method="post" class="comment-form">
			<p class="comment-form-comment">
				<label for="comment">
					<?php _e( 'Comment', 'prayers' ); ?> <a style="cursor: pointer;" id="<?php comment_ID(); ?>" class="prayer-js-cancel-comment">
						<?php _e( 'Cancel', 'prayers' ); ?>
					</a>
				</label>
				<p>
					<textarea cols="40" id="prayer-js-pray-reply-<?php comment_ID(); ?>" name="comment" required minlength="15"></textarea>
				</p>
			<?php if ( ! is_user_logged_in() ) { ?>
				<p class="comment-form-author">
					<label for="prayer-js-author-<?php comment_ID(); ?>"><?php _e( 'Name', 'prayers' ); ?></label>
					<input id="prayer-js-author-<?php comment_ID(); ?>" name="author" type="text" value="" size="30">
                </p>
                <?php /*
				<p><label for="prayer-js-email-<?php comment_ID(); ?>"><?php _e( 'Email', 'prayers' ); ?> </label>
					<input id="prayer-js-email-<?php comment_ID(); ?>" name="email" type="text" value="" size="30">
                </p>
				<p><label for="prayer-js-url-<?php comment_ID(); ?>"><?php _e( 'Website', 'prayers' ); ?></label>
					<input id="prayer-js-url-<?php comment_ID(); ?>" name="url" type="text" value="" size="30">
                </p>*/ ?>
			<?php } ?>
            <?php $upr_accept_privacy = get_option( 'upr_accept_privacy' ); 
            if ( $upr_accept_privacy != '' ) : ?>
                <p class="comment-form-accept-privacy">
                    <input type="checkbox" name="prayer-js-acceptprivacy-<?php comment_ID(); ?>" id="prayer-label-acceptprivacy-<?php comment_ID(); ?>" value="1">
                    <label for="prayer-label-acceptprivacy-<?php comment_ID(); ?>"><?php echo $upr_accept_privacy; ?></label>
                </p>
            <?php endif; ?>
			<p class="form-submit">
				<input name="submit" id="<?php comment_ID(); ?>" class="submit prayer-js-prayresponsreply"
					   value="<?php _e( 'Submit', 'prayers' ); ?>" type="button">
				<input name="comment_post_ID" value="<?php echo $comment->comment_post_ID; ?>"
					   id="comment_post_ID_<?php comment_ID(); ?>" type="hidden">
				<input name="comment_parent" id="comment_parent_<?php comment_ID(); ?>" value="<?php comment_ID(); ?>"
					   type="hidden">
            </p>
		</form>
	</div>
    <?php    
	if ( 'div' != $args['style'] ) : ?>
	</div>
<?php endif;
