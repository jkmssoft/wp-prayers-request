<?php
    // echo "<pre>"; print_r($comment);
	$upr_allow_comments_prayer_request = get_option( 'upr_allow_comments_prayer_request' );
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
	<<?php echo $tag; ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?> id="comment-<?php comment_ID(); ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
		<?php
		if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
		}
		?>
		<?php
		printf(
			__( '<cite class="fn">%s</cite> <span class="says">' . __( 'says', 'prayers' ) . ':</span>' ),
			// jkmssoft: hide author name if !upr_display_username_on_prayer_listing
			get_option( 'upr_display_username_on_prayer_listing' ) ? get_comment_author_link() : __( 'name hidden', 'prayers' )
		);
		?>
	</div>
	<?php if ( $comment->comment_approved == '0' ) : ?>
		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
		<br/>
	<?php endif; ?>

	<div class="comment-meta commentmetadata">
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
		<?php
		/* translators: 1: date, 2: time */
		printf(
			__( '%1$s at %2$s' ),
			get_comment_date(),
			get_comment_time()
		);
		?>
		</a>
		<?php
		edit_comment_link( __( '(Edit)' ), '  ', '' );
		?>
	</div>
	<?php comment_text(); ?>
	<?php
	$registration = get_option( 'comment_registration' );
	if ( $upr_allow_comments_prayer_request == 1 ) {
		if ( comments_open( $comment->comment_post_ID ) ) {
			if ( is_user_logged_in() ) {
				echo '<div class="reply"><a class="comment-reply-link pray-replay" id="' . $comment->comment_ID . '">' . __(
					'Reply',
					'prayers'
				) . '</a></div>';
			} else {
				if ( $registration == 1 ) :
					echo '<div class="reply"><a href="' . wp_login_url( $current_url ) . '" id="' . $current_url . '">' . __(
						'Log in to Reply',
						'prayers'
					) . '</a></div>';
				else :
					echo '<div class="reply"><a class="comment-reply-link pray-replay" id="' . $comment->comment_ID . '">' . __(
						'Reply',
						'prayers'
					) . '</a></div>';
				endif;
			}
		}
	}
    ?>
    <div id="reply_<?php comment_ID(); ?>" class="comment-respond" style="display:none">
		<form action="" method="post" class="comment-form">
			<p class="comment-form-comment">
				<label for="comment">
					<?php _e( 'Comment', 'prayers' ); ?> <a id="<?php comment_ID(); ?>" class="cancelcomment">
						<?php _e( 'Reply', 'prayers' ); ?>
					</a>
				</label>
				<p>
					<textarea id="pray_reply_<?php comment_ID(); ?>" name="comment" cols="45" rows="8" required minlength="15"></textarea>
				</p>
			<?php if ( ! is_user_logged_in() ) { ?>
				<p class="comment-form-author">
					<label for="author_<?php comment_ID(); ?>"><?php _e( 'Name', 'prayers' ); ?></label>
					<input id="author_<?php comment_ID(); ?>" name="author" type="text" value="" size="30">
                </p>
				<p><label for="email_<?php comment_ID(); ?>"><?php _e( 'Email', 'prayers' ); ?> </label>
					<input id="email_<?php comment_ID(); ?>" name="email" type="text" value="" size="30">
                </p>
				<p><label for="url_<?php comment_ID(); ?>"><?php _e( 'Website', 'prayers' ); ?></label>
					<input id="url_<?php comment_ID(); ?>" name="url" type="text" value="" size="30">
                </p>
			<?php } ?>
			<p class="form-submit">
				<input name="submit" id="<?php comment_ID(); ?>" class="submit prayresponsreply"
					   value="<?php _e( 'Submit', 'prayers' ); ?>" type="button">
				<input name="comment_post_ID" value="<?php echo $comment->comment_post_ID; ?>"
					   id="comment_post_ID_<?php comment_ID(); ?>" type="hidden">
				<input name="comment_parent" id="comment_parent_<?php comment_ID(); ?>" value="<?php comment_ID(); ?>"
					   type="hidden">
            </p>
		</form>
	</div>
    <?php
	if ( 'div' != $args['style'] ) : ?>
	</div>
<?php endif;
