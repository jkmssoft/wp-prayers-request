REST API
=

# Endpoints
GET /prayers/v1/prayers\
Optional GET parameter:
- paged: int (page number)
- type: string prophecies, prayers, witnesses

Returns array
- prayers: array of prayers:
    - prayer_id
	- type
	- title
	- date
	- content
	- prayers_count
	- comments: array
		- comment_ID
		- comment_author
		- comment_content
		- comment_date
- pagination array (optional, is only there if there are more than one pages):
	- pages
	- current_page

Return on error array: code, message\
-1: no data

POST /prayers/v1/add-comment\
POST Parameter:
- comment_author: string
- comment_author_email: string
- pray_reply: string
- comment_author_url: string
- prayer_id: int
- comment_parent: int (The parent comments ID)

Returns array: code, message, comment_id (on success)\
0: success\
-1: error

POST /prayers/v1/add-prayers\
POST Parameter:
- type: prophecies, witnesses, prayers (INFO: in GUI it is now changed prophecies to impressions and witnesses to hearing!)
- prayer_messages: string
- prayer_title: string
- prayer_author_name: string

Returns array: code, message\
 0: success\
-1: missing input\
-2: already exists\
-3: invalid type\
-4: type missing

POST /prayers/v1/pray\
POST Parameter:
- prayer_id

Returns array: code, message\
0: success\
-1: fail (invalid prayer_id)
