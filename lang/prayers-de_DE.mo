��    E      D  a   l      �     �     �  G        K     Z     a     j  B   p     �  e   �     !  �   '     �     �     �     �    �  H   �  
   (	     3	     K	     S	     g	     v	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     $
     )
     2
     @
     Q
  !   c
     �
     �
  	   �
  	   �
  	   �
     �
  	   �
     �
  E   �
  H   6  L     F   �  E     V   Y     �     �  �   �     z  *   �  $   �     �     �     �  �  �     �     �  Z   �     2  	   B  	   L     V  _   ^  	   �  �   �     K  �   R     �     �  	   �  !   �      Q   )     {     �  	   �     �     �     �     �     �               $     *     6     >     G     V     e     t  	   �     �     �     �     �     �     �     �  '   �  #        :     I     Z     h     |  	   �     �  M   �  J   �  P   D  P   �  I   �  _   0     �     �  �   �     y  0   �  %   �     �     �     �                8   :          1   0      A                6      +              D   	                   *   .       "   
          (   !   <   ,   &   @   #                  =   ?              $      )   %   4           E   B       >   3                               C   '                   ;   -   9       5                        /   7           2            of  %1$s at %2$s A prayer request with the same text already exists. So it is not saved! Ask for Prayer Cancel Category Click Click here to ask for prayer, give witness or share your prophecy. Comment Comment saved. If your comment is not directly visible, then it must first be activated by an editor! Email GOD told you something? Share it here! Spam protection: it will not be displayed directly, but an editor will publish it in a short time. Give Witness HERE Hearing Hide all prayed If you need prayer, you can simply fill out the following form. Then your prayer request will be displayed on the website and others can pray for your request (spam protection: it will not be displayed directly, but an editor will publish it in a short time). If you want to say thank you you can simply comment your prayer request. Impression Lowest Pray Count First Message New Prayer Requests New Prophecies New Witnesses Newest First No Prayer Requests found. Order By Page  Pray Pray Now Prayed Prayed  Prayer Count Prayer Request Prayers Prayers Performed Prophecy Reply Request Send Settings Share Hearing Share Impression Share as Prophecy Share prayer, hearing, impression Share prayer, witness, prophecy Show all Show less Show more Show/Hide Show/Hide Comments Statistic Submit Thank you. Your hearing has been received, it will be published soon. Thank you. Your impression has been received, it will be published soon. Thank you. Your prayer request has been received, it will be published soon. Thank you. Your prophecy has been received, it will be published soon. Thank you. Your witness has been received, it will be published soon. The query returned no elements, maybe there is nothing or your filters are too strict. This field is required Title We believe that prayer moves the heavens and thus has an impact on our lives. Here we want to share requests, witnesses and prophecies to make us as a church one in prayer. Witness You have to accept the privacy conditions! Your comment is awaiting moderation. name hidden says time(s) Project-Id-Version: Prayers System
PO-Revision-Date: 2017-04-25 01:55+0500
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.12
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: esc_html__;esc_html_e;__;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SearchPathExcluded-1: includes/options-page
  von  %1$s am %2$s Ein Gebetsanliegen mit dem selben Text existiert bereits. Daher wird es nicht gespeichert! Um Gebet bitten Abbrechen Kategorie klicken Klicken Sie hier, um um Gebet zu bitten, Zeugnis zu geben oder ihre Prophezeiung weiterzugeben. Kommentar Kommentar gespeichert. Wenn dein Kommentar nicht direkt sichtbar ist, dann muss er erst von einem Redakteur freigeschaltet werden! E-Mail GOTT hat dir etwas gesagt? Teile es hier! Spamschutz: es wird nicht direkt angezeigt, aber ein Redakteur wird es zügig freischalten. Zeugnis abgeben HIER Erhörung für bereits gebetetes ausblenden Wenn du Gebet benötigst, kannst du einfach das folgende Formular ausfüllen. Dann wird dein Gebetsanliegen auf der Website angezeigt und andere können für dein Anliegen beten (Spamschutz: es wird nicht direkt angezeigt, aber ein Redakteur wird es zügig freischalten). Wenn du dich bedanken willst, kannst du dein Gebetsanliegen einfach kommentieren. Eindruck Niedrigste Gebetsanzahl zuerst Nachricht Neue Gebetsanliegen Neue Prophetien Neue Zeugnisse Neueste zuerst Keine Gebetsanliegen gefunden. Sortieren nach Seite  beten Jetzt beten gebetet gebetet  Anzahl gebetet Gebetsanliegen Gebetsanliegen Gebete durchgeführt Prophetie kommentieren Anliegen Senden Einstellungen Teile Erhörung Teile Eindruck Prophetie teilen Teile Anliegen, Erhörungen, Eindrücke Anliegen, Zeugnis, Prophetie teilen Alles anzeigen Weniger anzeigen Mehr anzeigen Anzeigen/Ausblenden Kommentare anzeigen/verbergen Statistik senden Danke. Deine Erhörung ist eingegangen, sie wird bald veröffentlicht werden. Danke. Dein Eindruck ist eingegangen, er wird bald veröffentlicht werden. Danke. Dein Gebetsanliegen ist eingegangen, es wird bald veröffentlicht werden. Danke. Deine Prophezeiung ist eingegangen, sie wird bald veröffentlicht werden. Danke. Dein Zeugnis ist eingegangen, es wird bald veröffentlicht werden. Die Abfrage lieferte keine Elemente, vielleicht gibt es nichts oder ihre Filter sind zu streng. Dieses Feld ist erforderlich Titel Wir glauben daran, dass Gebet den Himmel bewegt und damit auch Auswirkungen auf unser Leben hat. Hier wollen wir Anliegen, Zeugnisse und Prophetien teilen um uns als Kirche im Gebet eins zu machen. Zeugnis Du musst die Datenschutzbedingungen akzeptieren! Dein Kommentar wartet auf Moderation. Name ausgeblendet sagt mal 